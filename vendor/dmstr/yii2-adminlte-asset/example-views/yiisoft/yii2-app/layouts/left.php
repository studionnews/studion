<?php
use mdm\admin\components\MenuHelper;
use yii\bootstrap\Nav;
use frontend\models\Assignment;

?>
<aside class="main-sidebar">
    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(


            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [


                    [
                        'label' => 'Category',
                        'icon' => 'book',
                        'url' => '#',

                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/category/create'],'visible' => Yii::$app->user->getIdentity('1') ? true : false],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/category/index'],],

                        ],
                    ],

                   /* [
                        'label' => 'States',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/states/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/states/index'],],

                        ],
                    ],*/

                    [
                        'label' => 'Districts',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/districts/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/districts/index'],],

                        ],
                    ],

                    [
                        'label' => 'Stickers',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/stickers/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/stickers/index'],],

                        ],
                    ],

                    [
                        'label' => 'News',
                        'icon' => 'newspaper-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/news/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/news/index'],],

                        ],
                    ],

                    [
                        'label' => 'Videos',
                        'icon' => 'video-camera',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/videos/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/videos/index'],],

                        ],
                    ],

                    [
                        'label' => 'Commerical advertise',
                        'icon' => 'buysellads',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/ads/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/ads/index'],],

                        ],
                    ],
                    [
                        'label' => 'Trending News',
                        'icon' => 'free-code-camp',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/trending-news/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/trending-news/index'],],

                        ],
                    ],
                    [
                        'label' => 'Gallary',
                        'icon' => 'picture-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/gallary/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/gallary/index'],],

                        ],
                    ],

                    [
                        'label' => 'Featured News',
                        'icon' => 'picture-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/feature/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/feature/index'],],

                        ],
                    ],

                    [
                        'label' => 'Pages',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            //['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/feature/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/pages/index'],],

                        ],
                    ],

                    /*[
                        'label' => 'Page Content',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create', 'icon' => 'plus-square', 'url' => ['/page-content/create'],],
                            ['label' => 'View', 'icon' => 'eye', 'url' => ['/page-content/index'],],

                        ],
                    ],*/


                    [
                        'label' => 'Settings',
                        'icon' => 'cogs',
                        'url' => '#',
                        'items' => [
                            ['label' => 'admin', 'icon' => 'plus-square', 'url' => ['/admin'],],
                            ['label' => 'route', 'icon' => 'eye', 'url' => ['/admin/route'],],
                            ['label' => 'permission', 'icon' => 'eye', 'url' => ['/admin/permission'],],
                            // ['label' => 'menu', 'icon' => 'eye', 'url' => ['/admin/menu'],],
                            ['label' => 'role', 'icon' => 'eye', 'url' => ['/admin/role'],],
                            ['label' => 'assignment', 'icon' => 'eye', 'url' => ['/admin/assignment'],],
                            ['label' => 'user', 'icon' => 'eye', 'url' => ['/admin/user'],],

                        ],
                        'visible' => Yii::$app->user->can('superadmin'),
                    ],

                    [
                        'label' => 'Users',
                        'icon' => 'user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create User', 'icon' => 'plus-square', 'url' => ['/admin/user/signup'],],
                            ['label' => 'Change Admin Password', 'icon' => 'eye', 'url' => ['/admin/user/change-password'],],
                        ],
                        'visible' => Yii::$app->user->can('superadmin'),
                    ],


                ],
            ]
        )



        ?>



    </section>

</aside>
