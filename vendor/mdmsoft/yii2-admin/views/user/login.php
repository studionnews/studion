<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

/*$this->title = Yii::t('rbac-admin', 'Login');
$this->params['breadcrumbs'][] = $this->title;*/
?>

<style type="text/css">.login-page, .register-page {
    background: #ffffff;

}
.login-box-body, .register-box-body {
    background: #dcdcdc;
    padding: 20px;
    border-top: 0;
    color: #666;</style>
<body class="hold-transition login-page">
<div class="login-box">
<div class="login-logo">
        <h1 class="logo"><img src="<?= Yii::getAlias('@web/theme') ?>/images/logo.png" alt=""/></h1>
      </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
      <div class="form-group has-feedback">


        <?= $form->field($model, 'username')->textInput(['placeholder' => "Enter Your Username",'class' => 'form-control'])->label(false); ?>
        
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Password",'class' => 'form-control'])->label(false); ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
             <div class="form-group">
                    <?= Html::submitButton(Yii::t('rbac-admin', 'Login'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
        
        </div>
        <!-- /.col -->
      </div>
     <?php ActiveForm::end(); ?>

 
                
  
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

