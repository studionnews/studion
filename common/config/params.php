<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    Yii::setAlias('@front', 'http://localhost/studion/frontend'),
    Yii::setAlias('@back', 'http://localhost/studion/backend'),
];
