<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'theme/css/bootstrap.min.css',
        'theme/css/style.css',
        'theme/css/blueimp-gallery.min.css',
        'theme/css/blueimp-gallery-indicator.css',
        'theme/css/blueimp-gallery-video.css',
        'theme/css/jquerysctipttop.css',


    ];
    public $js = [

       
		'theme/js/jquery-3.2.1.min.js',
		'theme/js/script.js',
        'theme/js/blueimp-gallery.min.js',
        'theme/js/blueimp-gallery-fullscreen.js',
        'theme/js/blueimp-gallery-indicator.js',
        'theme/js/blueimp-helper.js',
        'theme/js/jquery.blueimp-gallery.min.js',
        'theme/js/jquery-3.3.1.slim.min.js',

    ];
    public $depends = [
       // 'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
