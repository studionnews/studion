<?php

namespace frontend\widgets;
use Yii;
use yii\base\Widget;
use frontend\models\GallaryCategory;
use frontend\models\ArtistsData;


class gallarySectionWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $request = Yii::$app->request;
        $id = $request->get('id');
        $gallaryCat = GallaryCategory::find()->where(['id'=>$id,'gallary_cat_status'=>'ACTIVE'])->one();
        $catname= $gallaryCat->gallary_cat_name;
        $catid= $gallaryCat->id;


        return $this->render('gallarySectionWidget', [

            'catid'=>$catid,
            'catname'=>$catname,

        ]);
    }
}