<?php

namespace frontend\widgets;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;



class homePageJustWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $dataCat = new Category();
        $dataCat = $dataCat->getJustInData ();

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
    SELECT id, news_category, news_title,news_slug FROM tbl_news WHERE id IN ( SELECT MAX(id) FROM tbl_news GROUP BY news_category ORDER BY id DESC) LIMIT 9" );

        $result = $command->queryAll();
       // var_dump ($result);



        return $this->render('homePageJustWidget', [

            'dataCat' =>   $dataCat,
             'result' => $result,

        ]);
    }
}