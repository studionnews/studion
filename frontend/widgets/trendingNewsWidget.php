<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\TrendingNews;


class trendingNewsWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $trendingnews = TrendingNews::find()->where(['news_status'=>"active"])->limit(6)->orderBy(['created_timestamp' => SORT_DESC])->all();

        return $this->render('trendingNewsWidget', [

            'trendingnews' =>   $trendingnews,

        ]);
    }
}