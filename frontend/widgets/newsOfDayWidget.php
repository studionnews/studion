<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\News;


class newsOfDayWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $dataCat = new Category();
        $dataCat = $dataCat->getNewsOfDay();

        return $this->render('newsOfDayWidget', [

            'dataCat' =>   $dataCat,

        ]);
    }
}