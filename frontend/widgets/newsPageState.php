<?php

namespace frontend\widgets;
use Yii;
use frontend\models\Districts;
use app\models\States;
use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;


class newsPageState extends Widget
{
    public $message;
    public $categoryId;

    public function init()
    {
        parent::init();

    }

    public function run()
    {


        /*getting category names */
        $request = Yii::$app->request;
        $id =$this->categoryId;

        $dataCat = new States();
        $dataCat = $dataCat->getStateNews();

        $category = new Category();
        $category = $category->getStateNews(1);

        foreach ($dataCat as $statename){
            $state = $statename['id'];
            $statename = $statename['state_name'];
        }
        $parentid=$state;
        $name =$statename;
        $districts = Districts::find()->asArray()->where('state_id=:state_id',['state_id'=>$id])->orderBy(['id' => SORT_DESC])->all();


        return $this->render('newsPageState', [

            'dataCat' =>   $dataCat,
            'districts' =>   $districts,
            //'name' =>$name,
            'parentid'=>$parentid,
            'category' =>$category,

        ]);
    }
}