<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\GallaryCategory;
use frontend\models\ArtistsData;


class gallaryPageWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $gallaryCat = GallaryCategory::find()->where(['gallary_cat_status'=>'ACTIVE'])->all();


        return $this->render('gallaryPageWidget', [

            'gallaryCat' =>   $gallaryCat,

        ]);
    }
}