<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\TrendingNews;

class popularNewsWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {


        $trending = TrendingNews::find()->where(['news_status'=>"active"])->limit(4)->orderBy(['created_timestamp' => SORT_DESC])->all();
        

       return $this->render('popularNewsWidget', [
                       
                        'trending' =>   $trending,
             
                    ]);
    }
}