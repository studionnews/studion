<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\News;


class newsPageWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {

        $dataCat = new Category();
        $dataCat = $dataCat->getDataTabHomePage();

        $newsdata = News::find()->where(['news_status'=>"active"])->limit(1)->orderBy(['created_timestamp' => SORT_DESC])->all();
        $newsthumbs = News::find()->where(['news_status'=>"active"])->limit(4)->orderBy(['created_timestamp' => SORT_DESC])->offset(1)->all();

        return $this->render('newsPageWidget', [
                       
                        'newsdata' =>   $newsdata,
                        'newsthumbs' => $newsthumbs,
             
                    ]);
    }
}