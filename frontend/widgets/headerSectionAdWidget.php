<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\News;
use frontend\models\Ads;

class headerSectionAdWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $nedata = Ads::find()->where(['ad_status'=>"active",'ad_position'=>"HeaderSection1"])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->all();
        $headsection = Ads::find()->where(['ad_status'=>"active",'ad_position'=>"HeaderSection2"])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->all();

        return $this->render('headerSectionAdWidget', [
            'nedata' =>   $nedata,
            'headsection' =>   $headsection,
        ]);
    }
}