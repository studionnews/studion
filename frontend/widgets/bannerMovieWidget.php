<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\Category;


class bannerMovieWidget extends Widget
{
    public $message;
    public $categoryId;
   // public $categoryname;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $id=$this->categoryId;
        //$categoryname=$this->category_name_as;
        $dataCat = new Category();
        $dataCat = $dataCat->getBannersData($id);


        return $this->render('bannerMovieWidget', [

            'dataCat' =>   $dataCat,
            'id' =>   $this->categoryId,
          //  'categoryname' =>  $categoryname,

        ]);
    }
}