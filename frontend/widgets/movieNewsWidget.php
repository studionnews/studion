<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\Category;


class movieNewsWidget extends Widget
{
    public $message;
    public $categoryId;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $id = $this->categoryId;
        $dataCat = new Category();
        $dataCat = $dataCat->getMovieNew(3);
        return $this->render('movieNewsWidget', [

            'dataCat' =>   $dataCat,
            'id' =>$id,

        ]);
    }
}