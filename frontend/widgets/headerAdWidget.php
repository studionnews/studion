<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Ads;
use frontend\models\News;


class headerAdWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {

        $adsdata = Ads::find()->where(['ad_status'=>"active",'ad_position'=>"header"])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->all();

       return $this->render('headerAdWidget', [
                       
                        'adsdata' =>   $adsdata,
             
                    ]);
    }
}