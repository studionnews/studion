<?php

namespace frontend\widgets;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\Feature;


class featureTabWidget extends Widget
{
    public $message;
    public $categoryId;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $id = $this->categoryId;
        $dataCat = new Category();
        $dataCat = $dataCat->getSpecialNews();
        $featuredata = Feature::find()->where(['feature_status'=>"active",'category_id'=>$id])->limit(1)->orderBy(['created_timestamp' => SORT_DESC])->all();
        $featuredatas = Feature::find()->where(['feature_status'=>"active",'category_id'=>$id])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->offset(1)->all();
        $featuredata1 = Feature::find()->where(['feature_status'=>"active",'category_id'=>$id])->orderBy(['created_timestamp' => SORT_DESC])->limit(4)->offset(1)->all();


        return $this->render('featureTabWidget', [

            'dataCat' =>   $dataCat,
            'featuredata' => $featuredata,
            'featuredatas' => $featuredatas,
            'featuredata1' => $featuredata1,
            'category_id'=>$id,


        ]);
    }
}
