<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\News;


class categoryTabWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {

        $dataCat = new Category();
        $dataCat = $dataCat->getDataTabHomePage();

       return $this->render('categoryTabWidget', [
                       
                        'dataCat' =>   $dataCat,
             
                    ]);
    }
}