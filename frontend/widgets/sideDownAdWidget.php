<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Ads;
use frontend\models\News;


class sideDownAdWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $downdata = Ads::find()->where(['ad_status'=>"active",'ad_position'=>"SidebarDown"])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->all();

        return $this->render('sideDownAdWidget', [

            'downdata' =>   $downdata,

        ]);
    }
}