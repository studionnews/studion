<?php

namespace frontend\widgets;

use frontend\models\GallaryCategory;
use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\Gallary;
use frontend\models\Artists;


class homePageBottomWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {

      
    $dataCat = new Category();
    $dataCat = $dataCat->getSpecialNews();

    $categorydata = GallaryCategory::find()->limit(4)->orderBy(['created_timestamp' => SORT_DESC])->all();
    foreach ($categorydata as $catdata) {
        $gallerydata = Gallary::find()->where(['gallery_status' => "active"])->andWhere(['category_id'=>$catdata])->limit(1)->orderBy(['created_timestamp' => SORT_DESC])->all();
    }
    $gallerythumbs = Gallary::find()->where(['gallery_status'=>"active"])->limit(4)->orderBy(['created_timestamp' => SORT_DESC])->offset(1)->all();

        return $this->render('homePageBottomWidget', [
                       
                        'dataCat' =>   $dataCat,
                        'gallerydata' => $gallerydata,
                        'gallerythumbs' => $gallerythumbs,
                         'categorydata'=>$categorydata,
                      //  'artist' => $artist,


             
                    ]);
    }
}