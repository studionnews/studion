<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Ads;
use frontend\models\News;


class sideMiddleAdWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $thirddata = Ads::find()->where(['ad_status'=>"active",'ad_position'=>"SidebarMiddle"])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->all();

        return $this->render('sideMiddleAdWidget', [

            'thirddata' =>   $thirddata,

        ]);
    }
}