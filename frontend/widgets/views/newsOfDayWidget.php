<?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

 <?php
 foreach ($dataCat as $key => $value) { ?>


    <?php
    $new = new News();
    $new = $new->getDataTabimage($value['id']);
    if(isset($new)){?>
        <?php
    foreach ($new as $key => $newimage) {?>

        <div class="news-oftheday">
         <a title="<?php echo $newimage['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $newimage['news_slug']]);?>">
            <img src="<?php echo '/backend/images/resizeimages/news/newsofday'.'/'.$newimage['news_image4']; ?>" alt="<?php echo $newimage['news_title']?>"/>
            <div class="title-sec">
                <h2>News of the Day</h2>
            </div>
        </a> 
    </div>
    <?php } ?>
    <?php } ?>
</div>
 <?php } ?>

