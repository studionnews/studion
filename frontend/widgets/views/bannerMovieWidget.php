<?php
use frontend\models\Feature;
use yii\helpers\Html;
use yii\helpers\Url;
?>


<?php
foreach ($dataCat as $key => $value) {

    $new = new Feature();
    $new = $new->getBannerData($value['id']);
    foreach ($new as $banner) {
        $bannernews = $banner['category_id'];
    }
?>

        <div class="row inner-ban-sec">
            <?php foreach ($new as $banners){ ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="news-manual-main-sec"> <a title="<?php echo $banners['feature_title'];?>" href="<?php echo Url::toRoute(['feature/view','category'=>$value['category_name_as'], 'slug' => $banners['feature_slug']]);?>">
                            <img src="<?php echo '/backend/images/resizeimages/feature/poster'.'/'.$banners['feature_image3']; ?>" alt="<?php echo $banners['feature_title'];?>"/>
                            <h2><?php echo $banners['feature_title'];?></h2>
                        </a> </div>
                </div>
            <?php } ?>
        </div>
<?php } ?>