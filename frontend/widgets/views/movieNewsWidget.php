<?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="news-list-sec">
    <?php foreach ($dataCat as $key => $value) {?>
    <div class="heading-sec">

        <h4><?php echo $value['category_name'];?></h4>
    </div>
    <div class="row movie-grid">
        <?php
        $movies = new News();
        $movies = $movies->getMovie($value['id']);
        foreach ($movies as $key => $movie) {
        ?>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="movie-grid-main-sec">
                <a title="<?php echo $movie['news_title'] ?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $movie['news_slug']]);?>">
                    <img src="<?php echo '/backend/images/resizeimages/movies/'.'/'.$movie['news_image1']; ?>" alt="<?php echo $movie['news_title'] ?>"/>
                    <h3><?php echo mb_substr($movie['news_title'],0,120)?>..</h3>
                </a>
            </div>

            <?php } ?>
            <?php
            $moviesdata = new News();
            $moviesdata = $moviesdata->getMovies($value['id']);
            foreach ($moviesdata as $key => $moviesdatas) {

                $datee = $moviesdatas['created_timestamp'];
                $newdate = date ("Y-m-d",strtotime ($datee));
                $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
                $month=date("m",strtotime ($newdate));
                $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));

                ?>
                <div class="movie-grid-small-sec">
                    <a title="<?php echo $moviesdatas['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $moviesdatas['news_slug']]);?>">
                        <div class="movie-grid-img">
                            <img src="<?php echo '/backend/images/resizeimages/movies/thumbnails'.'/'.$moviesdatas['news_image1']; ?>" alt="<?php echo $moviesdatas['news_title']?>"/>
                        </div>
                        <div class="movie-grid-content">
                            <p><?php echo mb_substr($moviesdatas['news_title'],0,120)?>..</p>
                            <p><span></span><?php echo $newsdate;?></span></p>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?php
            $moviesdatalatest = new News();
            $moviesdatalatest = $moviesdatalatest->getMoviesData($value['id']);
            foreach ($moviesdatalatest as $key => $moviesdatalatests) {
                ?>
                <div class="movie-grid-main-sec">
                    <a title="<?php echo $moviesdatalatests['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $moviesdatalatests['news_slug']]);?>">
                        <img src="<?php echo '/backend/images/resizeimages/movies'.'/'.$moviesdatalatests['news_image1']; ?>" alt="<?php echo $moviesdatalatests['news_title']?>"/>
                        <h3><?php echo mb_substr($moviesdatalatests['news_title'],0,120)?>..</h3>
                    </a>
                </div>
            <?php } ?>
            <?php
            $moviesdate = new News();
            $moviesdate = $moviesdate->getMoviesDate($value['id']);
            foreach ($moviesdate as $key => $moviesdates) {
                $datee = $moviesdates['created_timestamp'];
                $newdate = date ("Y-m-d",strtotime ($datee));
                $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
                $month=date("m",strtotime ($newdate));
                $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));

                ?>
                <div class="movie-grid-small-sec">
                    <a title="<?php echo $moviesdates['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $moviesdates['news_slug']]);?>">
                        <div class="movie-grid-img">
                            <img src="<?php echo '/backend/images/resizeimages/movies/thumbnails'.'/'.$moviesdates['news_image1']; ?>" alt="<?php echo $moviesdates['news_title']?>"/>
                        </div>
                        <div class="movie-grid-content">
                            <p><?php echo mb_substr($moviesdates['news_title'],0,120)?>..</p>
                            <p><span><?php echo $newsdate;?></span></p>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>

        <?php }?>
        <a href="<?php echo Url::toRoute(['news/listpage', 'statelist' => $value['category_slug']]);?>" class="marinni-more">మరిన్ని »</a> </div>

        
</div>