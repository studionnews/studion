<?php
use frontend\models\News;
use yii\helpers\Url;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>

    <?php foreach ($category as $key => $value) { ?>
      

    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class="active1"><a href="#tab1default1" data-toggle="tab1"><?php echo $value['category_name'];?></a></li>
                <li class="dropdown">
                     <a href="#" data-toggle="dropdown" selected="selected">ఏ జిల్లా <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <?php
                        //$i=0;
                        foreach ($districts as $district){
                            /*$i++;
                            $class='';
                            if($i==1){
                                $class="active";

                            }*/
                            ?>
                        <li><a href="#<?php echo $district["id"];?>" data-toggle="tab1"><?php echo $district['district_name'];?></a></li>
                        <?php } ?>
                    </ul>
                    
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane fade active1 in" id="tab1default1">
                    <?php
                    $newscategorys = new News();
                    $newscategorys = $newscategorys->getStateNews($value["id"]);
                    foreach ($newscategorys as $key => $newscategory) {
                    $datee = $newscategory['created_timestamp'];
                       $newdate = date ("Y-m-d",strtotime ($datee));
                       $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
                       $month=date("m",strtotime ($newdate));
                       $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));
                               ?>
                    <div class="row news-grid">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="news-grid-main-sec"> <a href="<?php echo Url::toRoute(['news/view', 'slug' => $newscategory['news_slug']]);?>"> <img src="<?php echo '/backend/images'.'/'.$newscategory['news_image1']; ?>" alt=""/>
                                    <h2><?php echo $newscategory['news_title'];?></h2>
                                    <p><?php echo $newsdate;?></p>
                                </a> </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?php
                            $newscat = new News();
                            $newscat = $newscat->getStateNewsData($value["id"]);
                            foreach ($newscat as $key => $newscats) {
                            ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                    <div class="news-grid-small-sec"> <a href="<?php echo Url::toRoute(['news/view', 'slug' => $newscats['news_slug']]);?>"> <img src="<?php echo '/backend/images/resizeimages/districts/thumbs'.'/'.$newscats['news_image1']; ?>" alt=""/>
                                            <h4><?php echo $newscats['news_title'];?></h4>
                                        </a> </div>
                                </div>
                            <?php }?>
                        </div>
                       
                        <a href="<?php echo Url::toRoute(['news/listpage', 'id' => $newscategory['id']]);?>" class="marinni-more">మరిన్ని »</a> </div>
 <?php } ?>
                </div>
                <?php
               // $j=0;
                foreach ($districts as $district){
               /* $j++;
                $class='';
                if($j==1){
                    $class="active";

                }*/
                ?>




                <div class="tab-pane fade in" id="<?php echo $district["id"];?>">
                    <?php
                    $product = new News();
                    $product = $product->getstateNew($district["id"]);
                    foreach ($product as $key => $valuepro) {?>
                        <div class="row news-grid">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="news-grid-main-sec"> <a href="<?php echo Url::toRoute(['news/view', 'slug' => $valuepro['news_slug']]);?>"> <img src="<?php echo '/backend/images/'.'/'.$valuepro['news_image1']; ?>" alt=""/>
                                        <h2><?php echo $valuepro['news_title'];?></h2>
                                        <p>Nov 20 2018</p>
                                    </a> </div>
                            </div>



                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                $products = new News();
                                $products = $products->getstateNewss($district["id"]);
                                foreach ($products as $key => $value) {?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                    <div class="news-grid-small-sec"> <a href="<?php echo Url::toRoute(['news/view', 'slug' => $value['news_slug']]);?>"> <img src="<?php echo '/backend/images/resizeimages/districts/thumbs'.'/'.$value['news_image1']; ?>" alt=""/>
                                            <h4><?php echo $value['news_title'];?></h4>
                                        </a> </div>
                                </div>
                                <?php }?>
                            </div>



                        </div>
                    <?php }?>

                </div>
                <?php }?>

            </div>
        </div>

        <a href="<?php echo Url::toRoute(['news/listpage', 'id' => $value['id']]);?>" class="marinni-more">మరిన్ని »</a>

    </div>
    <?php } ?>

