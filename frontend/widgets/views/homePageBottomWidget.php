<?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="gal-special-stories">
  <div class="container">



    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <?php foreach ($gallerydata as $galleryimage){?>
        <h3>ఫొటోలు</h3>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padleft">
          <div class="gal-in-img gal-main-img"> 
            <a title="<?php echo $galleryimage['gallery_name']?>" href="gallery">
              <img src="<?php echo '/backend/images/resizeimages/gallery'.'/'.$galleryimage['gallery_image']; ?>" alt="<?php echo $galleryimage['gallery_name']?>"/>
            </a> 
          </div>
        </div>
          <?php } ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 gal-right">
           <?php foreach ($gallerythumbs  as $key => $value){?>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nopadding">
            <div class="gal-in-img"> 
              <a title="<?php echo $value['gallery_name']?>" href="gallery">
                <img src="<?php echo '/backend/images/resizeimages/gallery'.'/'.$value['gallery_image']; ?>" alt="<?php echo $value['gallery_name']?>"/>
              </a> 
            </div>
            <a class="mre" href="<?php //echo $value['category_slug'];?>">మరిన్ని »</a>
          </div>
            <?php } ?>
        </div>
      </div>


       <?php foreach ($dataCat as $key => $value) { ?>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <h3><?php echo $value["category_name"];?></h3>
          <?php
          $news = new News();
          $news = $news->getDataTabimage($value["id"]);
          foreach ($news as $key => $new) {
          ?>
        <div class="news-post"> 
          <a title="<?php echo $new['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $new['news_slug']]);?>">
            <img src="<?php echo '/backend/images/resizeimages/news/home'.'/'.$new['news_image1']; ?>" alt="<?php echo $new['news_title']?>"/>
          </a>
            <?php } ?>
          <ul>
             <?php
                            $news = new News();
                            $news = $news->getDataTabproduct($value["id"]);
                            foreach ($news as $key => $new) {
                            ?>
            <li><?= Html::a(Yii::t('app',mb_substr($new['news_title'],0,120)), ['news/view', 'slug' => $new['news_slug']]) ?>..</li>
            <?php } ?>
          </ul>

            <a class="mre" href="<?php echo $value['category_slug'];?>">మరిన్ని »</a> </div>
      </div>
    <?php } ?>

    </div>
  
  </div>
</div>