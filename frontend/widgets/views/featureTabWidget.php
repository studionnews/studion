<?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php
foreach ($dataCat as $date) {
    $divs =$date['id'];
};
if($category_id!=$divs){

?>


<div class="row inner-ban-sec">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <?php foreach ($featuredata as $feature){?>
        <div class="news-manual-main-sec"> 
            <a title="<?php echo $feature['feature_title']?>" href="<?php echo Url::toRoute(['feature/view', 'slug' => $feature['feature_slug']]);?>">
                <img src="<?php echo '/backend/images/resizeimages/feature/bigthumb'.'/'.$feature['feature_image1']; ?>" alt="<?php echo $feature['feature_title']?>"/>
                <h2><?php echo mb_substr($feature['feature_title'],0,120)?>..</h2>
            </a> 
        </div>
        <?php } ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

        <!--<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12 no-padding">
            <?php /*foreach ($featuredatas as $features){*/?>
            <div class="news-manual-small-sec"> 
                <a title="<?php /*echo $features['feature_title']*/?>" href="<?php /*echo Url::toRoute(['feature/view', 'slug' => $features['feature_slug']]);*/?>">
                    <img src="<?php /*echo '/backend/images/resizeimages/feature/bigthumb'.'/'.$features['feature_image']; */?>" alt="<?php /*echo $features['feature_title']*/?>"/>
                    <h3><?php /*echo substr($features['feature_title'],0,120)*/?>..</h3>
                </a> 
            </div>
            <?php /*} */?>
        </div>-->
            <?php foreach ($featuredata1 as $featuredata2){?>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
            <div class="news-manual-small-sec"> 
                <a title="<?php echo $featuredata2['feature_title']?>" href="<?php echo Url::toRoute(['feature/view', 'slug' => $featuredata2['feature_slug']]);?>">
                    <img src="<?php echo '/backend/images/resizeimages/feature/thumbnails'.'/'.$featuredata2['feature_image2']; ?>" alt="<?php echo $featuredata2['feature_title']?>"/>
                    <h3><?php echo mb_substr($featuredata2['feature_title'],0,40)?>..</h3>
                </a> 
            </div>
           
        </div>
         <?php } ?>
    </div>
</div>
<?php } ?>