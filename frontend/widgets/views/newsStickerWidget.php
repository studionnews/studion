<div class="flash-news">
    <div class="container">
        <span>FLASH NEWS <i class="fa fa-caret-right" aria-hidden="true"></i></span>
        <div class="marquee-studio">

            <div class="marquee--inner">
                
                <ul>
                    <?php foreach ($data as $key => $value) { ?>
                    <li><?php echo $value['sticker_title'];?></li>
                     <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>

