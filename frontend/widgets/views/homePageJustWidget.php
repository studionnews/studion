<?php
/**
 * Created by PhpStorm.
 * User: Fingoshop
 * Date: 21-12-2018
 * Time: 12:03
 */
use frontend\models\News;
use yii\helpers\Url;
?>




<div class="just-in-sec">
    <h3>Latest</h3>
    <ul>

        <?php
        //$news = new News();
        //$news = $news->getJustTabNews(1);
        foreach ($result as $key => $new) {
        ?>
        <li><a href="<?php echo Url::toRoute(['news/view', 'slug' => $new['news_slug']]);?>">
            <?php echo mb_substr($new['news_title'], 0,50)?>..</a></li>
        <?php } ?>
    </ul>


</div>


