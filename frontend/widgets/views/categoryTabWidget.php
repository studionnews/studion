<?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="second-sec">
    <div class="">
        <div class="row">

            <?php
            foreach ($dataCat as $key => $value) {
            $new = new News();
            $new = $new->getDataTabimage($value["id"]);
            if(count($new)!==0){ ?>


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="news-post">
                    <h3><?php echo $value['category_name'];?></h3>
                    <?php
                    foreach ($new as $key => $newimage) {
                    ?>
                    <a title="<?php echo $newimage['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $newimage['news_slug']]);?>">    <img src="<?php echo '/backend/images/resizeimages/news/home'.'/'.$newimage['news_image1']; ?>" alt="<?php echo $newimage['news_title']?>"/>
                    </a>
                    <?php } ?>
                    <ul>
                        <?php
                        $news = new News();
                        $news = $news->getDataTabproduct($value["id"]);
                        foreach ($news as $key => $new) {
                            ?>

                            <li><?= Html::a(Yii::t('app',mb_substr($new['news_title'], 0,30)), ['news/view', 'slug' => $new['news_slug']]) ?>..</li>
                        <?php } ?>
                    </ul>
                    <a class="mre" href="<?php echo $value['category_slug'];?>">మరిన్ని »</a> </div>
            </div>
            <?php }?>
            <?php } ?>


        </div>
    </div>
</div>

