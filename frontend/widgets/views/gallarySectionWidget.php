<?php
use frontend\models\Artists;
use frontend\models\Gallary;
use yii\helpers\Url;
?>

<?php
$gallarynews = new Artists();
$gallarynews = $gallarynews->geSectionGallaryImage($catid);

$this->params['breadcrumbs'][] = ['label' => $catname];
?>

<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2><?php $catname?></h2></div>
        <?php foreach ($gallarynews as $gallary){ ?>
            <?php  $gallarynew = new Gallary();

            $gallarynew = $gallarynew->getDataGallaryImages($gallary["id"]);?>
            <?php foreach ($gallarynew as $gallaryimage){  ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="gal-in-sec">
                        <a title="" href="<?php echo Url::toRoute(['site/gallary-list', 'id' => $gallaryimage['artist_data_id']]);?>"> <img src="<?php echo '/backend/images/resizeimages/gallary'.'/'.$gallaryimage['gallery_image']; ?>" alt=""/></a>
                        <h5><?php echo $gallary['artist_name'];?></h5>
                    </div>

                </div>
            <?php } ?>
        <?php } ?>
</div>
