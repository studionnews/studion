    <?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>

 <?php foreach ($dataCat as $key => $value) { ?>
<div class="latest-news">
  <div class="container">
    <h3><?php echo $value['category_name'];?></h3>
    <div class="row">
      <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
          <?php
          $news = new News();
          $news = $news->getDataLatests($value["id"]);
          foreach ($news as $key => $new) {
          ?>
        <div class="latest-news-main"> 
        	<a title="<?php echo $new['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $new['news_slug']]);?>">
        		<img src="<?php echo '/backend/images/resizeimages/news/latest/'.'/'.$new['news_image1']; ?>" alt="<?php echo $new['news_title']?>"/>
        	</a> 
        </div>
      </div>
        <?php } ?>
      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
         <?php
                            $news1 = new News();
                            $news1 = $news1->getDataLatest($value["id"]);
                            foreach ($news1 as $key => $new) {

                                $datee = $new['created_timestamp'];
                                $newdate = date ("Y-m-d",strtotime ($datee));
                                $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
                                $month=date("m",strtotime ($newdate));
                                $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));
                            ?>
        <div class="latest-news-thumb">
        	<a title="<?php echo $new['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $new['news_slug']]);?>">
          <div class="thumb-image"> 
          	<img src="<?php echo '/backend/images/resizeimages/news/latest/thumnails'.'/'.$new['news_image1']; ?>" alt="<?php echo $new['news_title']?>"/> 
          </div>
          <div class="thumb-content">
            <p><?php echo mb_substr($new['news_title'],0,120)?>..</p>
            <p><span><?php echo $newsdate;?></span></p>
          </div>
          </a> 
      </div>

          <?php } ?>
       
       
        <a class="mre" href="<?php echo $value['category_slug'];?>">మరిన్ని »</a> </div>
    </div>
  </div>
</div>
 <?php } ?>