<?php
use frontend\models\News;
use yii\helpers\Url;

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>

<?php foreach ($category as $key => $value) { ?>


    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1default" data-toggle="tab"><?php echo $value['category_name'];?></a></li>
                <li class="dropdown">
                    <?php if(count ($districts)!==0) { ?>  <a href="#" data-toggle="dropdown" id="a">ఏ జిల్లా <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php
                            foreach ($districts as $district){
                                ?>
                                <li><a href="#<?php echo $district["id"];?>" data-toggle="tab"><?php echo $district['district_name'];?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab1default">
                    <?php
                    $newscategorys = new News();
                    $newscategorys = $newscategorys->getStateNews($value["id"]);
                    foreach ($newscategorys as $key => $newscategory) {
                        $datee = $newscategory['created_timestamp'];
                        $newdate = date ("Y-m-d",strtotime ($datee));
                        $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
                        $month=date("m",strtotime ($newdate));
                        $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));
                        ?>
                        <div class="row news-grid">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="news-grid-main-sec">
                                    <a title="<?php echo $newscategory['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $newscategory['news_slug']]);?>">
                                        <img src="<?php echo '/backend/images/resizeimages/districts'.'/'.$newscategory['news_image1']; ?>" alt="<?php echo $newscategory['news_title']?>"/>
                                        <h2><?php echo mb_substr($newscategory['news_title'],0,40)?>..</h2>
                                        <p><?php echo $newsdate;?></p>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                $newscat = new News();
                                $newscat = $newscat->getStateNewsData($value["id"]);
                                foreach ($newscat as $key => $newscats) {

                                    ?>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                        <div class="news-grid-small-sec">
                                            <a title="<?php echo $newscats['news_title']?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $newscats['news_slug']]);?>">
                                                <img src="<?php echo '/backend/images/resizeimages/districts/thumbs'.'/'.$newscats['news_image1']; ?>" alt="<?php echo $newscats['news_title']?>"/>
                                                <h4><?php echo mb_substr($newscats['news_title'],0,40)?>..</h4>
                                            </a>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>

                            <a href="<?php echo Url::toRoute(['news/listpage', 'statelist' => $value['category_slug']]);?>" class="marinni-more">మరిన్ని »</a> </div>
                    <?php } ?>
                </div>
                <?php
                foreach ($districts as $district){
                    ?>




                    <div class="tab-pane fade in" id="<?php echo $district["id"];?>">
                        <?php
                        $product = new News();
                        $product = $product->getstateNew($district["id"]);
                        foreach ($product as $key => $valuepro) {


                            $todate = $valuepro['created_timestamp'];
                            $newdates = date ("Y-m-d",strtotime ($todate));
                            $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
                            $month=date("m",strtotime ($newdate));
                            $nowdate = $months[$month-1]." ".date("d,Y",strtotime ($newdates));

                            ?>
                            <div class="row news-grid">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="news-grid-main-sec"> <a href="<?php echo Url::toRoute(['news/view', 'slug' => $valuepro['news_slug']]);?>"> <img src="<?php echo '/backend/images/'.'/'.$valuepro['news_image1']; ?>" alt=""/>
                                            <h2><?php echo mb_substr($valuepro['news_title'],0,40)?>..</h2>
                                            <p><?php echo $nowdate;?></p>
                                        </a> </div>
                                </div>



                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <?php
                                    $products = new News();
                                    $products = $products->getstateNewss($district["id"]);
                                    foreach ($products as $key => $value) {?>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                                            <div class="news-grid-small-sec"> <a href="<?php echo Url::toRoute(['news/view', 'slug' => $value['news_slug']]);?>"> <img src="<?php echo '/backend/images/resizeimages/districts/thumbs'.'/'.$value['news_image1']; ?>" alt=""/>
                                                    <h4><?php echo mb_substr($value['news_title'],0,40)?>..</h4>
                                                </a> </div>
                                        </div>
                                    <?php }?>
                                </div>



                            </div>
                        <?php }?>
                        <a href="<?php echo Url::toRoute(['news/listpage', 'statelist' => $district['slug']]);?>" class="marinni-more">మరిన్ని »</a>
                    </div>
                <?php }?>

            </div>
        </div>



    </div>
<?php } ?>

