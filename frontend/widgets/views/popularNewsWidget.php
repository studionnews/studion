<?php
use frontend\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
?>
   <div class="most-list-news">



          <h4>ప్రముఖ వార్తలు</h4>
       <?php foreach ($trending as $trendings){

           $datee = $trendings['created_timestamp'];
           $newdate = date ("Y-m-d",strtotime ($datee));
           $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
           $month=date("m",strtotime ($newdate));
           $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));
           ?>
          <div class="most-list-news-main"> <a title="<?php echo $trendings->news_title ?>" href="<?php echo Url::toRoute(['trending-news/view', 'slug' => $trendings['news_slug']]);?>">
            <div class="most-list-news-thumb"> <img src="<?php echo '/backend/images/resizeimages/trending/thumbs'.'/'.$trendings['news_image1']; ?>" alt="<?php echo $trendings->news_title ?>"/> </div>
            <div class="most-list-news-content">
              <p><?php echo mb_substr($trendings["news_title"],0,120)?>..</p>
              <p><span><?php echo $newsdate;?></span></p>
            </div>
            </a>
          </div>
          <?php }?>
         
        
        </div>

