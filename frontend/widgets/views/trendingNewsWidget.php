<?php

use yii\helpers\Url;
?><div class="news-list-sec">
    <div class="heading-sec">
        <h4>ప్రముఖ వార్తలు</h4>
    </div>
    <div class="row news-grid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php foreach($trendingnews as $trendingnew){
            $datee = $trendingnew['created_timestamp'];
           $newdate = date ("Y-m-d",strtotime ($datee));
           $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
           $month=date("m",strtotime ($newdate));
           $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));
            ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 no-padding">
                <div class="news-grid-small-sec"> 
                    <a title="<?php echo $trendingnew['news_title']?>" href="<?php echo Url::toRoute(['trending-news/view', 'slug' => $trendingnew['news_slug']]);?>">
                        <img src="<?php echo '/backend/images/resizeimages/trending'.'/'.$trendingnew['news_image1']; ?>" alt="<?php echo $trendingnew['news_title']?>"/>
                        <h4><?php echo $trendingnew['news_title'];?></h4>
                        <p><?php echo $newsdate?></p>
                    </a> 
                </div>
            </div>
            <?php } ?>
        </div>
        <a href="#" class="marinni-more">మరిన్ని »</a> </div>
</div>