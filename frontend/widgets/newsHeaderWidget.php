<?php
/**
 * Created by PhpStorm.
 * User: Fingoshop
 * Date: 15-12-2018
 * Time: 17:38
 */

namespace frontend\widgets;


class newsHeaderWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $id = $this->categoryId;
        $dataCat = new Category();
        $dataCat = $dataCat->getSpecialNews();
        $featuredata = Feature::find()->where(['feature_status'=>"active",'category_id'=>$id])->limit(1)->orderBy(['created_timestamp' => SORT_DESC])->all();
        $featuredatas = Feature::find()->where(['feature_status'=>"active",'category_id'=>$id])->orderBy(['created_timestamp' => SORT_DESC])->limit(1)->offset(1)->all();
        $featuredata1 = Feature::find()->where(['feature_status'=>"active",'category_id'=>$id])->orderBy(['created_timestamp' => SORT_DESC])->limit(2)->offset(2)->all();




        return $this->render('newsHeaderWidget', [

            'dataCat' =>   $dataCat,
            'featuredata' => $featuredata,
            'featuredatas' => $featuredatas,
            'featuredata1' => $featuredata1,


        ]);
    }
}