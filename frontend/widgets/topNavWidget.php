<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;

class topNavWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {
        $category = Category::find ()->limit (10)->all ();

        $data = Category::find ()->asArray ()->orderBy (['menu_position' => SORT_ASC])->limit (11)->all ();

        return $this->render('topNavWidget',[

       		'data' => $data,
       ]);
    }
}