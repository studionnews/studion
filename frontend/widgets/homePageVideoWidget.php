<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Videos;
use frontend\models\News;


class homePageVideoWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {

        $videodata = Videos::find()->where(['video_status'=>"active",'video_homepage'=>"1"])->limit(1)->orderBy(['created_timestamp' => SORT_DESC])->all();

       return $this->render('homePageVideoWidget', [
                       
                        'videodata' =>   $videodata,
             
                    ]);
    }
}