<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Videos;
use frontend\models\News;


class popularVideosWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {

        $videodata = Videos::find()->where(['video_status'=>"active",'video_homepage'=>"0"])->limit(3)->orderBy(['created_timestamp' => SORT_DESC])->all();

       // var_dump($videodata);
    
       return $this->render('popularVideosWidget', [
                       
                        'videodata' =>   $videodata,
             
                    ]);
    }
}