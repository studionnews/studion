<?php
/**
 * Created by PhpStorm.
 * User: Fingoshop
 * Date: 21-12-2018
 * Time: 11:46
 */


namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Category;
use frontend\models\News;


class homePageHeaderSectionWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $dataCat = new Category();
        $dataCat = $dataCat->getDataTabHomePage();

        return $this->render('homePageHeaderSectionWidget', [

            'dataCat' =>   $dataCat,

        ]);
    }
}