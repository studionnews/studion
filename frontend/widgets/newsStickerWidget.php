<?php

namespace frontend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Stickers;



class newsStickerWidget extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $data = Stickers::find ()->asArray ()->orderBy (['id' => SORT_DESC])->limit (50)->all();

        return $this->render('newsStickerWidget', [

            'data' =>   $data,

        ]);
    }
}
