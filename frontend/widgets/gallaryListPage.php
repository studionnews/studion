<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\models\Gallary;



class gallaryListPage extends Widget
{
    public $message;

    public function init()
    {
        parent::init();

    }

    public function run()
    {

        $artist = Gallary::find()->where(['artist_data_id'=>'ACTIVE'])->all();
        return $this->render('gallaryListPage', [

            'artist' =>   $artist,

        ]);
    }
}