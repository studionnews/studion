<?php

namespace frontend\components;

use yii\web\UrlRuleInterface;
use yii\base\BaseObject;
use frontend\models\CustomRewriteUrls;

class CustomUrlRule extends BaseObject implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {

        if(isset($params['slug']))
        {
            return $params['slug'];
        }

        if(isset($params['category']))
        {
            $page="";
            if($params['page'])
            {
                $page="/".$params['page'];
            }
            return $params['category']."/news".$page;
        }
        if(isset($params['statelist']))
        {
            return $params['statelist']."/news";
        }



        return false; // this rule does not apply
    }

    public function parseRequest($manager, $request)
    {
        
        $pathInfo = $request->getPathInfo();

        $pathParams=explode('/', $pathInfo);

        if(in_array('news',$pathParams))
        {
            $params['category']=$pathParams[0];
            if(isset($pathParams[2]))
            {
                $params['page']=$pathParams[2];
            }            
            return ["news/listpage", $params];
        }
        $url=CustomRewriteUrls::find()->where(['rewrite_url'=>$pathInfo])->one();
        if ($url) {
            $params['slug']=$pathInfo;
            return [$url->entity, $params];
        }
        return false; // this rule does not apply
    }
}