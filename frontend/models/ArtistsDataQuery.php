<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[ArtistsData]].
 *
 * @see ArtistsData
 */
class ArtistsDataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ArtistsData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ArtistsData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
