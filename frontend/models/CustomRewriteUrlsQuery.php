<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[CustomRewriteUrls]].
 *
 * @see CustomRewriteUrls
 */
class CustomRewriteUrlsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CustomRewriteUrls[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CustomRewriteUrls|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
