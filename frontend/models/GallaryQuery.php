<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[Gallary]].
 *
 * @see Gallary
 */
class GallaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Gallary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Gallary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
