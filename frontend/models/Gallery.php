<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_gallery}}".
 *
 * @property int $id
 * @property string $gallery_name
 * @property string $gallery_image
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_gallery}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gallery_name', 'gallery_image', 'created_by','gallery_status'], 'required'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['gallery_name', 'gallery_image', 'created_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'gallery_name' => Yii::t('app', 'Gallery Name'),
            'gallery_image' => Yii::t('app', 'Gallery Image'),
            'gallery_status' => Yii::t('app', 'Gallery Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return GalleryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GalleryQuery(get_called_class());
    }

}
