<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_artists_data}}".
 *
 * @property int $artist_data_id
 * @property string $artist_data_name
 * @property int $artist_id
 * @property string $artist_data_status
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class ArtistsData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_artists_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist_data_name', 'artist_id','artist_category', 'artist_data_status'], 'required'],
            [['artist_id'], 'integer'],
            [['artist_data_status'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['artist_data_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Artist Data ID'),
            'artist_data_name' => Yii::t('app', 'Artist Data Name'),
            'artist_category' => Yii::t('app', 'Artist Category'),
            'artist_id' => Yii::t('app', 'Artist ID'),
            'artist_data_status' => Yii::t('app', 'Artist Data Status'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ArtistsDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArtistsDataQuery(get_called_class());
    }



    public function getArtist()
    {
        return $this->hasOne(Artists::className(), ['id' => 'id']);
    }




    public function getDataGallaryImage($artistid,$limit = 4){

        $data = ArtistsData::find()->asArray()->where('artist_id=:artistid',['artistid'=>$artistid])->andwhere(['artist_data_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function getArtistsData($artistid){
        $data = ArtistsData::find()->asArray()->where('artist_id=:artistid',['artistid'=>$artistid])->andwhere(['artist_data_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

}
