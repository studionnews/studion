<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GallaryCategory;

/**
 * GallaryCategorySearch represents the model behind the search form of `app\models\GallaryCategory`.
 */
class GallaryCategorySearch extends GallaryCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gallary_cat_id'], 'integer'],
            [['gallary_cat_name', 'gallary_cat_status', 'created_timestamp', 'updated_timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GallaryCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'gallary_cat_id' => $this->gallary_cat_id,
            'created_timestamp' => $this->created_timestamp,
            'updated_timestamp' => $this->updated_timestamp,
        ]);

        $query->andFilterWhere(['like', 'gallary_cat_name', $this->gallary_cat_name])
            ->andFilterWhere(['like', 'gallary_cat_status', $this->gallary_cat_status]);

        return $dataProvider;
    }
}
