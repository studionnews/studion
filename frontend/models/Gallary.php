<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_gallary}}".
 *
 * @property int $gallery_id
 * @property int $artist_id
 * @property int $artist_data_id
 * @property string $gallery_name
 * @property string $gallery_image
 * @property string $gallery_status
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Gallary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_gallary}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //  [['artis], 'required'],
            [['artist_id'], 'integer'],
            [['gallery_status'], 'string'],
            [['created_timestamp', 'updated_timestamp', 'artist_data_id'], 'safe'],
            ['created_by', 'default', 'value' =>  Yii::$app->user->identity->username],
            [['gallery_name'], 'string', 'max' => 200],
            [['gallery_image'], 'file', 'extensions' => 'jpeg, jpg, gif, png',/*'minWidth' => 719,'minHeight' => 719,*/ 'maxFiles'=>10,'skipOnEmpty' => true],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'gallery_id' => Yii::t('app', 'Gallery ID'),
            'artist_id' => Yii::t('app', 'Artist Name'),
            'artist_data_id' => Yii::t('app', 'Artist Related Data'),
            'gallery_name' => Yii::t('app', 'Gallery Name'),
            'gallery_image' => Yii::t('app', 'Gallery Image'),
            'gallery_status' => Yii::t('app', 'Gallery Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return GallaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GallaryQuery(get_called_class());
    }


    public function getDataGallaryImages($artistdataid,$limit = 1){

        $data = Gallary::find()->asArray()->where('artist_data_id=:artistdataid',['artistdataid'=>$artistdataid])->andwhere(['gallery_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function getGallayListImage($id){

        $data = Gallary::find()->asArray()->where('artist_data_id=:artistdataid',['artistdataid'=>$id])->andwhere(['gallery_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

    public function getGallaySectionImage($id){

        $data = Gallary::find()->asArray()->where('artist_data_id=:artistdataid',['artistdataid'=>$id])->andwhere(['gallery_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

    /* public function getGallayListImage($id){

         $data = Gallary::find()->asArray()->where('artist_data_id=:artistdataid',['artistdataid'=>$id])->andwhere(['gallery_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->all();
         return $data;
     }*/


    public function getGallaryImages($artistdataid,$limit = 1){

        $data = Gallary::find()->asArray()->where('artist_id=:artistdataid',['artistdataid'=>$artistdataid])->andwhere(['gallery_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

}
