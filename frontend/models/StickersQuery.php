<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[Stickers]].
 *
 * @see Stickers
 */
class StickersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Stickers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Stickers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
