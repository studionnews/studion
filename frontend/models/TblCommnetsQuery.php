<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Commnets]].
 *
 * @see Commnets
 */
class TblCommnetsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Commnets[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Commnets|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
