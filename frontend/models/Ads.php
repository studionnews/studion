<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_ads}}".
 *
 * @property int $id
 * @property string $ad_name
 * @property string $ad_url
 * @property string $ad_image
 * @property string $ad_google
 * @property string $ad_status
 * @property string $ad_position
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_ads}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ad_name', 'ad_url', 'ad_image', 'ad_google', 'ad_status', 'ad_position', 'created_by'], 'required'],
            [['ad_url', 'ad_google', 'ad_status', 'ad_position'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['ad_name', 'ad_image', 'created_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ad_name' => Yii::t('app', 'Ad Name'),
            'ad_url' => Yii::t('app', 'Ad Url'),
            'ad_image' => Yii::t('app', 'Ad Image'),
            'ad_google' => Yii::t('app', 'Ad Google'),
            'ad_status' => Yii::t('app', 'Ad Status'),
            'ad_position' => Yii::t('app', 'Ad Position'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AdsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdsQuery(get_called_class());
    }
}
