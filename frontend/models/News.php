<?php

namespace frontend\models;

use Yii;
//use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "{{%tbl_news}}".
 *
 * @property int $id
 * @property string $news_title
 * @property string $news_content
 * @property string $news_image1
 * @property string $news_image2
 * @property string $news_image3
 * @property string $news_status
 * @property int $news_category
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_news}}';
    }

   /* public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'news_slug',
            ],
        ];
    }*/

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_title', 'news_content', 'news_image1', 'news_image2', 'news_image3', 'news_status', 'news_category', 'created_by'], 'required'],
             [['news_title', 'news_content', 'news_status','news_excerpt','news_slug','news_video'], 'string'],
            [['news_category'], 'integer'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['news_image1', 'news_image2', 'news_image3','news_image4', 'created_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'news_title' => Yii::t('app', 'News Title'),
            'news_content' => Yii::t('app', 'News Content'),
            'news_slug' => Yii::t('app', 'News Slug'),
            'news_video' => Yii::t('app', 'Video link'),
            'news_excerpt' => Yii::t('app', 'News excerpt'),
            'news_image1' => Yii::t('app', 'News Image1'),
            'news_image2' => Yii::t('app', 'News Image2'),
            'news_image3' => Yii::t('app', 'News Image3'),
            'news_image4' => Yii::t('app', 'News Image3'),
            'news_status' => Yii::t('app', 'News Status'),
            'news_category' => Yii::t('app', 'News Category'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

     public function getDataTabproduct($catid,$limit = 1){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
      
    return $data;
    }


     public function getDataTabimage($catid=10,$limit = 1){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
    return $data;
    }


     public function getDataTablist($catid,$limit = 3){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
    return $data;
    }


     public function getDatalistimage($catid,$limit = 1){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
    return $data;
    }


     public function getDataNews($catid){
      $data = News::find()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->one();
    return $data;
    }

     public function getDataLatest($catid,$limit = 3){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->offset(1)->all();
    return $data;
    }

    public function getDataLatests($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


     public function getPopularNew($catid,$limit = 4){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
    return $data;
    }

     public function getSpecialNew($catid,$limit = 1){
      $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
    return $data;
    }


    public function getstateNew($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_districts	=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function getstateNewss($catid,$limit = 4){
        $data = News::find()->asArray()->where('news_districts	=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->offset(1)->all();
        return $data;
    }


    public function getdistrictNew($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_districts	=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->andwhere(['districts_default'=>'1'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function getNewsOfDay($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function getMovie($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

    public function getMoviesData($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->offset(4)->all();
        return $data;
    }


    public function getMovies($catid,$limit = 3){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->offset(1)->all();
        return $data;
    }

    public function getMoviesDate($catid,$limit = 3){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->offset(5)->all();
        return $data;
    }


    public function getStateNews($catid,$limit = 1){
        $data = News::find()->asArray()->where('news_category	=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

    public function getStateNewsData($catid,$limit = 4){
        $data = News::find()->asArray()->where('news_category	=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->offset(1)->all();
        return $data;
    }


    public function getBannerData($catid,$limit = 4){
        $data = News::find()->asArray()->where('news_category	=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }

    public function getJustTabNews($catid,$limit = 9){
        $data = News::find()->asArray()->where('news_category=:catid',['catid'=>$catid])->andwhere(['news_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();

        return $data;
    }








}
