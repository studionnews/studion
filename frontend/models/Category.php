<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string $category_name
 * @property string $category_name_as
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'category_name_as',], 'required'],
            [['category_name', 'category_name_as'], 'string'],
             [['created_timestamp', 'updated_timestamp','parent_id','position','menu_position','menu_option'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_name' => Yii::t('app', 'Category Name'),
            'category_name_as' => Yii::t('app', 'Category Name As'),
            'menu_position' => Yii::t('app', 'Menu Position'),
            'parent_id' => Yii::t('app', 'parent id'),
            'position' => Yii::t('app', 'Position'),

            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }


    public function getDataTabHomePage($parentid=0,$limit = 3){

        $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_DESC])->all();
      return $data;
    }


     public function getDataList($parentid=1,$limit = 3){
      $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_DESC])->all();
    return $data;
    }


    public function getDataLatest($parentid=2,$limit = 3){
      $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_DESC])->all();
    return $data;
    }

    public function getPopularNews($parentid=4,$limit = 1){
      $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_DESC])->all();
    return $data;
    }

    public function getSpecialNews($parentid=3,$limit = 1){
      $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_DESC])->all();
    return $data;
    }

    public function getNewsOfDay($parentid=5,$limit = 1){
        $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_DESC])->all();
        return $data;
    }


    public function getMovieNews($parentid=0,$limit = 1){
        $data = Category::find()->asArray()->where('parent_id=:parentid',['parentid'=>$parentid])->limit($limit)->orderBy(['position' => SORT_ASC])->all();

        return $data;
    }

    public function getMovieNew($limit = 1){
        $data = Category::find()->asArray()->where('inner_page=:id',['id'=>1])->limit($limit)->orderBy(['position' => SORT_ASC])->all();
        return $data;
    }

    public function getStateNews($id,$limit = 1){
        $data = Category::find()->asArray()->where('id=:id',['id'=>$id])->limit($limit)->orderBy(['position' => SORT_DESC])->all();

        return $data;
    }

    public function getBannersData($id,$limit = 1){

        $data = Category::find()->asArray()->where('id=:id', ['id'=>$id])->limit($limit)->orderBy(['position' => SORT_ASC])->all();
        return $data;
    }

    public function getJustInData(){

        $data = Category::find()->asArray()->orderBy(['position' => SORT_ASC])->all();
        return $data;
    }



}
