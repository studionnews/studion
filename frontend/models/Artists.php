<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_artists}}".
 *
 * @property int $artist_id
 * @property string $artist_name
 * @property string $artist_status
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Artists extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_artists}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist_name', 'artist_status','artist_category'], 'required'],
            [['artist_status'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['artist_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Artist ID'),
            'artist_name' => Yii::t('app', 'Artist Name'),
            'artist_status' => Yii::t('app', 'Artist Status'),
            'artist_category' => Yii::t('app', 'Artist Category'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ArtistsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArtistsQuery(get_called_class());
    }



    public function getDataTabimage($catid,$limit = 4){
        $data = Artists::find()->asArray()->where('artist_category=:catid',['catid'=>$catid])->andwhere(['artist_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();

        return $data;
    }


    public function getDataGallaryImage($artistid,$limit = 4){

        $data = Artists::find()->asArray()->where('artist_category=:artistid',['artistid'=>$artistid])->andwhere(['artist_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function geSectionGallaryImage($artistid){

        $data = Artists::find()->asArray()->where('artist_category=:artistid',['artistid'=>$artistid])->andwhere(['artist_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public static function getSubCatList($cat_id)
    {
        $subCategories = self::find()
            ->select('id','artist_name')
            ->where(['artist_category'=>$cat_id])
            ->asArray()
            ->all();

        return $subCategories;
    }


    public function getArtistsData($artistid,$limit = 4){
        $data = Artists::find()->asArray()->where('artist_category=:artistid',['artistid'=>$artistid])->andwhere(['artist_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }


    public function getArtistsList($artistid){
        $data = Artists::find()->asArray()->where('artist_category=:artistid',['artistid'=>$artistid])->andwhere(['artist_status'=>'ACTIVE'])->orderBy(['created_timestamp' => SORT_DESC])->all();
        return $data;
    }





}
