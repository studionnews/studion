<?php

namespace frontend\models;
use yii\behaviors\SluggableBehavior;


use Yii;

/**
 * This is the model class for table "{{%tbl_pages}}".
 *
 * @property int $id
 * @property string $page_name
 * @property string $page_slug
 * @property string $status
 * @property int $position
 * @property string $created_at
 * @property string $updated_at
 */
class Pages extends \yii\db\ActiveRecord
{
    const ENTITY="site/";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_pages}}';
    }

     public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['page_name'],
                'slugAttribute' => 'page_slug'
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $rewriteurl = RewriteUrls::find()->where(['entity' => self::ENTITY,'entity_id'=>$this->id])->one();
        if(empty($rewriteurl)) {
            $rewriteurl= new RewriteUrls();
        }
        $rewriteurl->rewrite_url = $this->page_slug;
        $rewriteurl->entity = self::ENTITY;
        $rewriteurl->entity_id =$this->id;
        $rewriteurl->status =$this->status;
        $rewriteurl->save (false);
        return parent::afterSave($insert, $changedAttributes);

    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_name', 'page_slug', 'status', 'position'], 'required'],
            [['status'], 'string'],
            [['position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['page_name', 'page_slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_name' => Yii::t('app', 'Page Name'),
            'page_slug' => Yii::t('app', 'Page Slug'),
            'status' => Yii::t('app', 'Status'),
            'position' => Yii::t('app', 'Position'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TblPagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TblPagesQuery(get_called_class());
    }
}
