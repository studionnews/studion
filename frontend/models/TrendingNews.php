<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_trending_news}}".
 *
 * @property int $id
 * @property string $news_title
 * @property string $news_excerpt
 * @property string $news_content
 * @property string $news_slug
 * @property string $news_image1
 * @property string $news_image2
 * @property string $news_image3
 * @property string $news_status
 * @property int $news_category
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class TrendingNews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_trending_news}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_title', 'news_excerpt', 'news_content', 'news_slug', 'news_image1', 'news_image2', 'news_image3', 'news_status', 'news_category', 'created_by'], 'required'],
            [['news_title', 'news_excerpt', 'news_content', 'news_slug', 'news_status'], 'string'],
            [['news_category'], 'integer'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['news_image1', 'news_image2', 'news_image3', 'created_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'news_title' => Yii::t('app', 'News Title'),
            'news_excerpt' => Yii::t('app', 'News Excerpt'),
            'news_content' => Yii::t('app', 'News Content'),
            'news_slug' => Yii::t('app', 'News Slug'),
            'news_image1' => Yii::t('app', 'News Image1'),
            'news_image2' => Yii::t('app', 'News Image2'),
            'news_image3' => Yii::t('app', 'News Image3'),
            'news_status' => Yii::t('app', 'News Status'),
            'news_category' => Yii::t('app', 'News Category'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TrendingNewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrendingNewsQuery(get_called_class());
    }
}
