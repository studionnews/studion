<?php

namespace frontend\models;

use Yii;


/**
 * This is the model class for table "{{%tbl_feature}}".
 *
 * @property int $id
 * @property string $feature_title
 * @property string $feature_excerpt
 * @property string $feature_content
 * @property string $feature_slug
 * @property string $feature_image
 * @property string $feature_status
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Feature extends \yii\db\ActiveRecord
{



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_feature}}';
    }





    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feature_title', 'feature_content', 'feature_slug', 'feature_image', 'feature_status', 'created_by'], 'required'],
            [['feature_title', 'feature_excerpt', 'feature_content', 'feature_slug', 'feature_status','meta_title','meta_description','meta_keywords'],  'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['feature_image', 'created_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'feature_title' => Yii::t('app', 'Feature Title'),
            'feature_excerpt' => Yii::t('app', 'Feature Excerpt'),
            'feature_content' => Yii::t('app', 'Feature Content'),
            'feature_slug' => Yii::t('app', 'Feature Slug'),
            'feature_image' => Yii::t('app', 'Feature Image'),
            'feature_status' => Yii::t('app', 'Feature Status'),
            'category_id' => Yii::t('app', 'category'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FeatureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeatureQuery(get_called_class());
    }

    public function getBannerData($catid,$limit = 4){
        $data = Feature::find()->asArray()->where('category_id=:catid',['catid'=>$catid])->andwhere(['feature_status'=>'ACTIVE'])->limit($limit)->orderBy(['created_timestamp' => SORT_DESC])->all();
       
        return $data;
    }
}
