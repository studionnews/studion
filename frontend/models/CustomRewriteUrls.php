<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_rewrite_urls}}".
 *
 * @property int $id
 * @property string $rewrite_url
 * @property string $entity
 * @property int $entity_id
 * @property int $status
 */
class CustomRewriteUrls extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_rewrite_urls}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rewrite_url', 'entity', 'entity_id', 'status'], 'required'],
            [['rewrite_url'], 'string'],
            [['entity_id', 'status'], 'integer'],
            [['entity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rewrite_url' => Yii::t('app', 'Rewrite Url'),
            'entity' => Yii::t('app', 'Entity'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CustomRewriteUrlsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomRewriteUrlsQuery(get_called_class());
    }
}
