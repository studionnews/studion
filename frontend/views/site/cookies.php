<?php


if(!empty($content)){
	$this->title = $content->meta_title;
}else{
	$this->title = 'Cookies';
}

?>

<div class="container">
	<?php if(!empty($content)){
		echo $content->content;
	}?>
</div>
