<?php 
use yii\helpers\Url;
use frontend\models\Gallary;
use frontend\widgets\gallarySectionWidget;

$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['site/gallary']];

?>
    <div class="body-in-sec gallery-sec">
        <div class="container">


            <?= gallarySectionWidget::widget() ?>


        </div>
    </div>

