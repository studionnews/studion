﻿<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use frontend\widgets\topNavWidget;

AppAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Studio N</title>
    <link href="<?= Yii::getAlias('@web/theme') ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= Yii::getAlias('@web/theme') ?>/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php $this->beginBody() ?>

<div class="header-top-strip">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="today-date">
          <p><?= date('l, M d, Y')?></p>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <div class="signin-signup">
        <a href="<?php echo Url::toRoute(['site/login']);?>">Sign In</a> 
        <a href="<?php echo Url::toRoute(['site/signup']);?>">Sign Up</a> 
      </div>
       </div>
    </div>
  </div>
</div>
<?= topNavWidget::widget() ?>


    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<div class="footer-full-width">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-links">
        <ul>
          <li><a href="#"> Terms of Use  |</a></li>
          <li><a href="#"> About Us  |</a></li>
          <li><a href="#"> Privacy Policy  |</a></li>
          <li><a href="#"> Cookies  |</a></li>
          <li><a href="#"> Accessibility  |</a></li>
          <li><a href="#"> Help  |</a></li>
          <li><a href="#"> Parental  |</a></li>
          <li><a href="#"> Guidance  |</a></li>
          <li><a href="#"> Contact Us  |</a></li>
          <li><a href="#"> Get Personalised  |</a></li>
          <li><a href="#"> Newsletters  |</a></li>
          <li><a href="#"> Advertise with us  |</a></li>
          <li><a href="#"> Ad choices
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 left-sec">
        <p>© <?= date('Y') ?> studio N</p>
      </div>

<style type="text/css">
  a {
    color: #ffffff;
}
</style>
   


      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 right-sec">
        <a href="http://creotechsolutions.net/" target="_blank"> <p>Developed By : Creotechsolutions Pvt Ltd</p></a>
      </div>
    </div>
  </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
