<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>
<br><br>
    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>
<br><br><br><br>
    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>
