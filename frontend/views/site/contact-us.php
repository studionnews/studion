<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

if(!empty($content)){
    $this->title = $content->meta_title;
}else{
    $this->title = 'Contact Us';
}
//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <h4>Contact Us</h4>
            <p>If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.</p>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'subject') ?>

            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                <?= $form->field($model, 'body')->textarea(['rows' => 4]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
            </div>

                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
