<?php 

use frontend\widgets\categoryTabWidget;
use frontend\widgets\latestNewsWidget;
use frontend\widgets\bottomListWidget;
use frontend\widgets\homePageVideoWidget;
use frontend\widgets\homePageBottomWidget;
use frontend\widgets\newsOfDayWidget;
use frontend\widgets\newsStickerWidget;
use frontend\widgets\homePageHeaderSectionWidget;
use frontend\widgets\headerSectionAdWidget;

if(!empty($content)){
  $this->title = $content->meta_title;
}else{
  $this->title = 'Cookies';
}

?>
<?= newsStickerWidget::widget() ?>
<!--<div class="first-sec">
  <div class="container">
      <div class="row">

  <?/*= newsOfDayWidget::widget() */?>

      </div>
  </div>
 </div>-->


<div class="first-sec">
    <div class="container">
        <div class="row total-section">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 section-one">
                <?= \frontend\widgets\homePageJustWidget::widget()  ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 section-two">
                <?= homePageVideoWidget::widget()
                ?>

                <?= categoryTabWidget::widget()  ?>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 section-three">
                <?= headerSectionAdWidget::widget()  ?>
            </div>
        </div>
    </div>
</div>

</div>
</div>


<?= latestNewsWidget::widget() ?>
<?= bottomListWidget::widget() ?>
<?= homePageBottomWidget::widget() ?>
<div class="weather-sec">
  <div class="container">
      <a class="weatherwidget-io" href="https://forecast7.com/en/17d3978d49/hyderabad/" data-label_1="HYDERABAD" data-label_2="Telangana" data-theme="original" >HYDERABAD Telangana</a>
      <script>
          !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
      </script>
  </div>
</div>
</br>