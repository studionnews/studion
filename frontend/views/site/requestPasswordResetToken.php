<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
//$this->params['breadcrumbs'][] = $this->title;
?>


<div class="body-in-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-sign-up-sec-main">
        <div class="login-sign-up-sec">
          <h5><img src="/backend/theme/images/small-logo.png" alt=""/> SIGN IN</h5>
          <p class="p">Reset your password here.</p>
          <div class="inpt-sec">
            <p>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                
                <?= $form->field($model, 'email')->textInput(['autofocus' => true , 'required'=>true,'class' => 'inpt', 'placeholder' => "Enter your email"])->label(false); ?>
               
            
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                
            <?php ActiveForm::end(); ?>
        </p>

       </div>
        </div>
      </div>
    </div>
  </div>
</div>
