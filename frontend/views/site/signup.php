<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="body-in-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-sign-up-sec-main">
        <div class="login-sign-up-sec">
          <h5><img src="/backend/theme/images/small-logo.png" alt=""/> SIGN UP</h5>
          <p class="p">Welcome! Register Here</p>
          <div class="inpt-sec">

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true ,'class'=>'inpt', 'placeholder' => "Your User Name" , 'required'=>true])->label(false); ?>

                <?= $form->field($model, 'email')->textInput(['class'=>'inpt','placeholder' => "Your Email-id", 'required'=>true])->label(false); ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => "Your Password",'class'=>'inpt' , 'required'=>true])->label(false); ?>

               
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
               You Already Registered go to <a href="login">Sign in</a>
            <?php ActiveForm::end(); ?>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>