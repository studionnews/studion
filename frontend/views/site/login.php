<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="body-in-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-sign-up-sec-main">
        <div class="login-sign-up-sec">
          <h5><img src="/backend/theme/images/small-logo.png" alt=""/> SIGN IN</h5>
          <p class="p">Welcome! Log into your account</p>
          <div class="inpt-sec">
            <p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'required'=>true ,'class' => 'inpt', 'placeholder' => "Your User Name"])->label(false); ?>
                <?= $form->field($model, 'password')->passwordInput(['class'=>'inpt','required'=>true, 'placeholder'=>"Your Password"])->label(false); ?>
            
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                Forgot your password? <?= Html::a(' Get help ', ['site/request-password-reset']) ?>

            <?php ActiveForm::end(); ?>
        </p>

       </div>
        </div>
      </div>
    </div>
  </div>
</div>