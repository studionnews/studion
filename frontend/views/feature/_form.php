<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Feature */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feature-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'feature_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'feature_excerpt')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'feature_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'feature_slug')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'feature_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'feature_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'DEACTIVE' => 'DEACTIVE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_timestamp')->textInput() ?>

    <?= $form->field($model, 'updated_timestamp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
