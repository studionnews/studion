<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\widgets\popularNewsWidget;
use frontend\widgets\headerAdWidget;
use frontend\widgets\sidebarAdWidget;
use frontend\widgets\popularVideosWidget;
use kartik\rating\StarRating;
use frontend\widgets\sideMiddleAdWidget;
use frontend\widgets\sideDownAdWidget;
use yii\widgets\ActiveForm;


use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model frontend\models\News */

$this->title = $model->meta_title;
/*$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/


?>
<div class="body-in-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="detail-sec-in">
            <?= Yii::$app->session->getFlash('review-create') ?>
            <?php
            $datee = $model['created_timestamp'];
            $newdate = date ("Y-m-d",strtotime ($datee));
            $months=['జనవరి','ఫిబ్రవరి','మార్చి','ఏప్రిల్','మే','జూన్','జూలై','ఆగస్టు','సెప్టెంబర్','అక్టోబర్','నవంబర్','డిసెంబర్'];
            $month=date("m",strtotime ($newdate));
            $newsdate = $months[$month-1]." ".date("d,Y",strtotime ($newdate));

            ?>
          
          <h1><?php echo $model->feature_title;?></h1>
          <p><span><?php echo $newsdate;?></span></p>
          <div class="detail-main-image"> <img src="<?php echo '/backend/images/resizeimages/feature'.'/'.$model['feature_image']; ?>"> </div>
          <div class="detail-content">
            <p><?php echo $model->feature_content;?></p>
          </div>

          <div class="sharethis-inline-share-buttons"></div>
         <br>
         <?php //print_r($model); exit(); ?>
         <!--Comments Section Start-->
        <div class="comment-sec">
            <div class="comment-head">
              <h4><?php $count = count ( $commnets ); echo $count;?> Comments</h4>
              <!-- <div class="sort">
                <label>Sort By :
                  <select>
                    <option>Latest Comments</option>
                    <option>Previous Comments</option>
                  </select>
                </label>
              </div> -->
            </div>
            <div class="comments-list">
            <?php $form = ActiveForm::begin(['action'=>Yii::$app->urlManager->createUrl(['site/feature-comment'])]); ?>
            <?= $form->field($model1, 'name')->textInput(['required' => true,'class'=>'comment-inpt' ,'placeholder'=>'Name'])->label(false) ?>
            <?= $form->field($model1, 'comments')->textInput(['required' => true,'class'=>'comment-inpt' ,'placeholder'=>'Comments'])->label(false) ?>
        <div class="container">
            <div class="rating-star" id="rating-star">
                <span data-val="1"></span>
                <span data-val="2"></span>
                <span data-val="3"></span>
                <span data-val="4"></span>
                <span data-val="5"></span>
            </div>
        </div>
        <?= $form->field($model1, 'rating')->hiddenInput()->label(false) ?>
           <?php /*?>
            <?php 
            echo $form->field($model1, 'rating')->widget(StarRating::classname(), [
            'pluginOptions' => ['size'=>'lg','class'=>'inpt2', 'required']
              ])->label(false);
            ?>
            
            <?php 
            echo StarRating::widget([
          'name' => 'rating',
          'pluginOptions' => ['disabled'=>true, 'showClear'=>false]
            ]);
            ?>
           <?php */?>
          <?= $form->field($model1, 'slug')->hiddenInput(['value' => $model->feature_slug ])->label(false) ?>
          <?= $form->field($model1, 'post_id')->hiddenInput(['value' => $model->id ])->label(false) ?>
          <?= $form->field($model1, 'category_id')->hiddenInput(['value' => $model->category_id ])->label(false) ?>
          <?= $form->field($model1, 'parent_id')->hiddenInput(['value'=>0])->label(false) ?>
          <?= $form->field($model1, 'status')->hiddenInput(['value' => 'ACTIVE' ])->label(false) ?>
        <p><?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn','onclick'=>'return check();']) ?></p>
    <?php ActiveForm::end(); ?>

              <div class="comments-list-in">
                <ul>
                  <?php foreach($commnets as $commnets){ ?>
                  <li><img src="/backend/images/profile.jpg"/>
                    <p><strong><?php echo $commnets['name']; ?></strong></p>
                    <p><?php echo $commnets['comments']; ?></p>
                    <p><?php
                    for ($a = 1; $a <= 5; $a++) 
                    {
                        if ($a <= round($commnets->rating)) 
                        {?>
                          <span class="star star-fill "> <i class="fa fa-star" aria-hidden="true"></i> </span>
                          <?php
                        }else 
                        {?>
                          <span class="star"> <i class="fa fa-star" aria-hidden="true"></i> </span>
                          <?php
                        }
                    }
                    ?>
                    <span>( <?php echo $commnets->rating; ?> / 5)</span>
                  </p>
                    <p>
                      <span>
                      <?php 
                      Yii::$app->formatter->locale = 'en-US'; 
                      echo Yii::$app->formatter->asDate($commnets['created_at']) ?>
                      </span>
                      <span>
                      <?php 
                      Yii::$app->formatter->locale = 'en-US'; 
                      echo Yii::$app->formatter->asTime($commnets['created_at']) ?>
                      </span>
                    </p>
                    <span><a href="#" class="show_hide">Reply Comment</a>
                    <div class="slidingDiv">
                      <form>
                        <textarea class="comment-inpt" placeholder="Comments"></textarea>
                        <input class="btn" type="submit"/>
                      </form>
                    </div>
                    </span>
                   </li>
                <?php } ?>
                  <!-- <li><img src="images/profile.jpg"/>
                    <p><strong>Sandeepkatakam Sandys</strong></p>
                    <p>Had a nice time watching the episodes......loved every episode nice humor and good web series in recent time...</p>
                    <p><span>May 6, 2017</span> <span>9:14am</span></p>
                    <span><a href="#" class="show_hide">Reply Comment</a>
                    <div class="slidingDiv">
                      <form>
                        <textarea class="comment-inpt" placeholder="Comments"></textarea>
                        <input class="btn" type="submit"/>
                      </form>
                    </div>
                    </span></li> -->
                  <!-- <li><img src="images/profile.jpg"/>
                    <p><strong>Sandeepkatakam Sandys</strong></p>
                    <p>Had a nice time watching the episodes......loved every episode nice humor and good web series in recent time...</p>
                    <p><span>May 6, 2017</span> <span>9:14am</span></p>
                    <span><a href="#" class="show_hide">Reply Comment</a>
                    <div class="slidingDiv">
                      <form>
                        <textarea class="comment-inpt" placeholder="Comments"></textarea>
                        <input class="btn" type="submit"/>
                      </form>
                    </div>
                    </span></li> -->
                </ul>
              </div>
            </div>
          </div>
          <!--Comments Section End-->


          <div class="related-news">
            <div class="related-news-in">
              <h3>సంబందిత వార్తలు</h3>
              <?php foreach ($Newsdata as $key => $value) { ?>
                  
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="news-post"> <a title="<?php echo $value["feature_title"];?>" href="<?php echo Url::toRoute(['feature/view', 'slug' => $value['feature_slug']]);?>"><img src="<?php echo '/backend/images/resizeimages/feature/thumbnails'.'/'.$value['feature_image2']; ?>" alt="<?php echo $value["feature_title"];?>"/></a>
                  <ul>
                    <li><?= Html::a(Yii::t('app',substr($value['feature_title'],0,40)), ['feature/view', 'slug' => $value['feature_slug']]) ?>..</a></li>
                   <!--  <li><a href="#"><?php echo $value["feature_excerpt"];?></a></li> -->
                   </li>
                  </ul>
                </div>
              </div>

            <?php } ?>

            </div>
           
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
       

        <?= sidebarAdWidget::widget() ?>
        <?= popularNewsWidget::widget() ?>
         <?= sideMiddleAdWidget::widget() ?>
         <?= popularVideosWidget::widget() ?>
<?= sideDownAdWidget::widget() ?>

      
      </div>
    </div>
  </div>
</div>



