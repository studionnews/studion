<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TrendingNewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trending-news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'news_title') ?>

    <?= $form->field($model, 'news_excerpt') ?>

    <?= $form->field($model, 'news_content') ?>

    <?= $form->field($model, 'news_slug') ?>

    <?php // echo $form->field($model, 'news_image1') ?>

    <?php // echo $form->field($model, 'news_image2') ?>

    <?php // echo $form->field($model, 'news_image3') ?>

    <?php // echo $form->field($model, 'news_status') ?>

    <?php // echo $form->field($model, 'news_category') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_timestamp') ?>

    <?php // echo $form->field($model, 'updated_timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
