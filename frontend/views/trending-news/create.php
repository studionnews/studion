<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TrendingNews */

$this->title = 'Create Trending News';
$this->params['breadcrumbs'][] = ['label' => 'Trending News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trending-news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
