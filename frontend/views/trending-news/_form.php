<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TrendingNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trending-news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'news_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'news_excerpt')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'news_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'news_slug')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'news_image1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_image2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_image3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'news_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'DEACTIVE' => 'DEACTIVE', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'news_category')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_timestamp')->textInput() ?>

    <?= $form->field($model, 'updated_timestamp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
