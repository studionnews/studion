<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TrendingNewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trending News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trending-news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trending News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'news_title:ntext',
            'news_excerpt:ntext',
            'news_content:ntext',
            'news_slug:ntext',
            //'news_image1',
            //'news_image2',
            //'news_image3',
            //'news_status',
            //'news_category',
            //'created_by',
            //'created_timestamp',
            //'updated_timestamp',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
