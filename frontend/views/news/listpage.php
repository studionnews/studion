<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use frontend\widgets\popularNewsWidget;
use frontend\widgets\headerAdWidget;
use frontend\widgets\sidebarAdWidget;
use frontend\widgets\popularVideosWidget;
use frontend\widgets\sideMiddleAdWidget;
use frontend\widgets\sideDownAdWidget;

if(!empty($content)){
  $this->title = $content->meta_title;
}else{
  $this->title = 'List';
}


//$this->title = "Hello";
?>
<div class="body-in-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="list-sec-in">
          <h3><?php echo $catdata['category_name']?></h3>
            <?php foreach ($articles as $article){ ?>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">


            <div class="news-post">
                <a title="<?php echo $article->news_title?>" href="<?php echo Url::toRoute(['news/view', 'slug' => $article['news_slug']]);?>">
                  <img src="<?php echo '/backend/images/resizeimages/news/listing'.'/'.$article['news_image3']; ?>" alt="<?php echo $article->news_title?>"/>
                </a>
              <ul>

                <li><a href="<?php echo Url::toRoute(['news/view', 'slug' => $article['news_slug']]);?>"><?php echo mb_substr($article['news_title'],0,180)?>..</a></li>

              </ul>
            </div>

          </div>

            <?php } ?>
        </div>



        <div class="pagination-list">
            <?= LinkPager::widget([

                    'pagination' => $pagination,
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                ]) ?>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= sidebarAdWidget::widget() ?>
        <?= popularNewsWidget::widget() ?>
        <?= sideMiddleAdWidget::widget() ?>
        <?= popularVideosWidget::widget() ?>
        <?= sideDownAdWidget::widget() ?>
      </div>
    </div>
  </div>
</div>