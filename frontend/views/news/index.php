<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\widgets\newsPageWidget;
use frontend\widgets\newsPageState;
use frontend\widgets\newsPageStates;
use frontend\widgets\featureTabWidget;
use frontend\widgets\trendingNewsWidget;
use frontend\widgets\popularNewsWidget;
use frontend\widgets\movieNewsWidget;
use frontend\widgets\sidebarAdWidget;
use frontend\widgets\popularVideosWidget;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'News';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="body-in-sec">
  <div class="container">
      <?= newsPageWidget::widget() ?>
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="list-sec-in">
          <div class="news-list-sec">

            <div class="row news-grid">
                <?= newsPageState::widget() ?>
              <a href="#" class="marinni-more">మరిన్ని »</a> </div>
          </div>
          <div class="news-list-sec">
          
            <div class="row news-grid">
               <?= newsPageStates::widget() ?>
             
            
              <a href="#" class="marinni-more">మరిన్ని »</a> </div>
          </div>
          <div class="news-list-sec">
              <?= trendingNewsWidget::widget() ?>

          </div>
          <div class="news-list-sec">
              <?= movieNewsWidget::widget() ?>
          </div>


          </div>
        </div>

      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <?= sidebarAdWidget::widget() ?>
        <div class="most-list-news">


            <?= popularNewsWidget::widget() ?>
        </div>
        <div class="add-sec"> <a href="#"><img src="images/nexa-add.jpg" alt=""/></a> </div>
          <?= popularVideosWidget::widget() ?>
        
      </div>
    </div>
  </div>
</div>
