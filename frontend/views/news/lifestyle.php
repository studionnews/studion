<?php
use frontend\widgets\stateNewsWidget;
use frontend\widgets\featureTabWidget;
use frontend\widgets\trendingNewsWidget;
use frontend\widgets\popularNewsWidget;
use frontend\widgets\movieNewsWidget;
use frontend\widgets\sidebarAdWidget;
use frontend\widgets\popularVideosWidget;
use frontend\widgets\bannerMovieWidget;
use frontend\widgets\sideMiddleAdWidget;
use frontend\widgets\sideDownAdWidget;
//use frontend\widgets\newsHeaderWidget;
if(!empty($content)){
  $this->title = $content->meta_title;
}else{
  $this->title = 'Life Style';
}
?>
<div class="container">
    <?php if($categoryname!=="Movies")
    {  ?>
    <?= featureTabWidget::widget(['categoryId'=>$categoryid]) ?>
    <?php } ?>
    <?php if($categoryname==="Movies")
    {  ?>
    <?= bannerMovieWidget::widget(['categoryId'=>$categoryid]) ?>
    <?php } ?>

    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="list-sec-in">
            <?php if($categoryname!=="Movies")
            {  ?>
            <?= stateNewsWidget::widget(['categoryId'=>$categoryid]) ?>
            <?php } ?>
            <?= movieNewsWidget::widget() ?>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <?= sidebarAdWidget::widget() ?>
         <?= popularNewsWidget::widget() ?>
          <?= sideMiddleAdWidget::widget() ?>
         <?= popularVideosWidget::widget() ?>
          <?= sideDownAdWidget::widget() ?>

          </div>
    </div>
  </div>
</div>
