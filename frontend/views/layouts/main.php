<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use frontend\widgets\topNavWidget;

use frontend\models\Pages;

AppAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= Html::csrfMetaTags() ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script|ZCOOL+XiaoWei" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web/theme') ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= Yii::getAlias('@web/theme') ?>/css/style.css" rel="stylesheet" type="text/css"/>
    <script src="//platform-api.sharethis.com/js/sharethis.js#property=5bf548121d4aa900114390e6&product=inline-share-buttons"></script>

</head>
<body onload="startTime()">
<?php $this->beginBody() ?>
<div class="header-full-sec">
<div class="header-top-strip">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="today-date">
          <p><?= date('l, M d, Y') .' '.'<span id="txt"></span>'?></p>
        </div>
      </div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="signin-signup"> 

          <?php if (Yii::$app->user->isGuest) { ?>
          <a class="signin" href="<?php echo Url::toRoute(['site/login']);?>">Sign In</a>
          <a class="signup" href="<?php echo Url::toRoute(['site/signup']);?>">Sign Up</a>
          <?php }else{
            ?>
            <?= Html::beginForm(['/site/logout'], 'post') ?>
            <?= Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ') ',
            ['class' => 'btn btn-link logout']
            ) ?>
            <?= Html::endForm() ?>

            <?php
          } ?>
        </div>
      </div>
  
    </div>
  </div>
</div>




<?= topNavWidget::widget() ?>


    
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    
</div>

<div>
  
  </div>

<div class="footer-full-width">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sub-links">
        <ul>
        	<?php $page = Pages::find()->where(['status'=>1])->orderBy(['position' => SORT_DESC])->all(); 
            foreach ($page as $key => $value) { ?>
            <li><a href="<?php echo Url::base(true) .'/'.  $value->page_slug ?>"> <?php echo $value->page_name ?> | </a></li>
            <?php } ?>
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 left-sec">
        <p>© <?= date('Y') ?> studio N</p>
      </div>

<style type="text/css">
  a {
    color: #ffffff;
}
</style>
   


      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 right-sec">
        <a href="http://www.creotechsolutions.net/" target="_blank"> <p>Developed By : Creotechsolutions Pvt Ltd</p></a>
      </div>
    </div>
  </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>



