<?php
use frontend\models\Artists;
use frontend\models\Gallary;
use yii\helpers\Url;
$this->title="Gallery";
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['gallery/index']];

?>
<div class="body-in-sec gallery-sec">
    <?php
    $artistsdatas = new \frontend\models\ArtistsData();
   $artistsdatas = $artistsdatas->getArtistsData($artists->id);

   //var_dump($artists->id);
    ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2><?php echo $artists->artist_name;?></h2>
                <?php $this->params['breadcrumbs'][] = ['label' => $artists->artist_name ]; ?>
            </div>
            <?php foreach ($artistsdatas as $artistsdata){
                $gallarynews = new Gallary();
                $gallarynews = $gallarynews->getGallaryImages($artists->id);
                foreach ($gallarynews as $gallarynew){?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="gal-in-sec">
                    <a title="" href="<?php echo Url::toRoute(['gallery/gallery-section', 'id' => $artistsdata['id']]);?>"> <img src="<?php echo '/backend/images/resizeimages/gallary'.'/'.$gallarynew['gallery_image']; ?>" alt=""/></a>
                    <h3><?php echo $artistsdata['artist_data_name'];?></h3>
                </div>
            </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

</div>

