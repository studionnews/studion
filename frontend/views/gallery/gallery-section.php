<?php
use frontend\models\Gallary;
$this->params['breadcrumbs'][] = ['label' => 'Gallery', 'url' => ['site/gallary']];
$this->params['breadcrumbs'][] = ['label' => $artistsdataname->id];
?>
<div class="body-in-sec gallery-sec">
    <div class="container">
        <?php $images = new Gallary();
$images = $images->getGallayListImage($artistsdataname->id);
?>

<div id="links" class="row">
    <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h2><?php echo $artistsdataname->artist_data_name;?></h2>  </div>
    <?php foreach ($images as $image){ ?>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="gal-in-sec">
                <a title="" href="<?php echo '/backend/images/resizeimages/gallary'.'/'.$image['gallery_image']; ?>">
                    <img src="<?php echo '/backend/images/resizeimages/gallary'.'/'.$image['gallery_image']; ?>" alt=""/></a>
            </div>
        </div>
    <?php } ?>
</div>
    </div>
</div>



<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<script>
    document.getElementById('links').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };
</script>
<script>
    blueimp.Gallery(
        document.getElementById('links').getElementsByTagName('a'),
        {
            container: '#blueimp-gallery-carousel',
            carousel: true
        }
    );
</script>