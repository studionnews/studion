<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Feature;
use frontend\models\FeatureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Category;
use yii\data\Pagination;
use frontend\models\Comments;


/**
 * FeatureController implements the CRUD actions for Feature model.
 */
class FeatureController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feature models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeatureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feature model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        $model1=new Comments();
        $relateddata = Feature::find()->where(['feature_slug'=>$slug])->one();
        $commnets = Comments::find()->where(['category_id'=>$relateddata['category_id'], 'post_id'=>$relateddata['id'],'status'=>'ACTIVE'])->all();
        // $relateddata =  $this->findModel($id);
         $cat_id=$relateddata->category_id;
        $post_id=$relateddata["id"];

         $Newsdata = Feature::find()->where('category_id=:category_id and id != :id',['category_id'=>$cat_id,'id'=>$post_id])->andWhere (['feature_status'=>"active"])->orderBy(['id' => SORT_DESC])->limit(9)->all();
         if ($relateddata->meta_description) {
                \Yii::$app->view->registerMetaTag([
                    'name' => 'description',
                    'content' => $relateddata->meta_description
                ]);
            }
            if ($relateddata->meta_keywords) {
                \Yii::$app->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => $relateddata->meta_keywords
                ]);
            }

        return $this->render('view', [
            'model' =>$relateddata,
             'Newsdata' => $Newsdata,
             'model1' =>$model1,
            'commnets'=>$commnets
        ]);
    }

    /**
     * Creates a new Feature model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feature();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Feature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Feature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Feature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feature::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
