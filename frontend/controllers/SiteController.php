<?php
namespace frontend\controllers;

use app\models\TblGallaryCategory;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Bussiness;
use frontend\models\News;
use frontend\models\PageContent;
use frontend\models\Pages;
use frontend\models\Artists;
use yii\helpers\Url;
use frontend\models\ArtistsData;
use frontend\models\GallaryCategory;
use frontend\models\Comments;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
      /*Yii::$app
            ->mailer
            ->compose ('emailWithdrawUser',["email"=>"rathish@ctouchproducts.com", "username"=>"Rathish Kumar Kommu"])
            ->setFrom ([Yii::$app->params['supportEmail'] => 'Bookwow'])
            ->setTo (["rathish@ctouchproducts.com"])
            ->setSubject ('Studio N home page Visit')
            ->send ();*/

      $pageID=Pages::find()->where(['page_slug'=>'home'])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('index',[
        'content'=>$content,
      ]);
      }
      return $this->render('index',[
        //'content'=>$content,
      ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
    
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

   
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionGallary(){

       /* $category = GallaryCategory::find()->where(['gallary_cat_status'=>"active"])->all();
        foreach ($category as $cat){
            $catid =$cat["id"];
            var_dump($catid);
            $data = Artists::find()->asArray()->where('artist_category=:artistid',['artistid'=>$catid])->andwhere(['artist_status'=>'ACTIVE'])->limit(4)->orderBy(['created_timestamp' => SORT_DESC])->all();
            var_dump($data);
            exit();
        }*/



        return $this->render('gallary',[

           // 'category'=>$category,
        ]);
    }

    public function actionGallaryList(){

        return $this->render('gallary-list',[
        ]);
    }
    
    public function actionGallarySection($id){




    		return $this->render('gallary-section',[

        ]);
    }

    

    //footer page actions start
    public function actionTermsOfUse(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('terms-of-use',[
        'content'=>$content,
      ]);
      }
    	return $this->render('terms-of-use',[
    		//'content'=>$content,
    	]);
    }
    public function actionAboutUs(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('about-us',[
        'content'=>$content,
      ]);
      }
    	return $this->render('about-us',[
    		//'content'=>$content,
    	]);
    }
    public function actionPrivacyPolicy(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('privacy-policy',[
        'content'=>$content,
      ]);
      }
    	return $this->render('privacy-policy',[
    		//'content'=>$content,
    	]);
    }
    public function actionCookies(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('cookies',[
        'content'=>$content,
      ]);
      }
    	return $this->render('cookies',[
    		//'content'=>$content,
    	]);
    }
    public function actionAccessibility(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('accessibility',[
        'content'=>$content,
      ]);
      }
    	return $this->render('accessibility',[
    		//'content'=>$content,
    	]);
    }
    public function actionHelp(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('help',[
        'content'=>$content,
      ]);
      }
    	return $this->render('help',[
    		//'content'=>$content,
    	]);
    }
    public function actionParental(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('parental',[
        'content'=>$content,
      ]);
      }
    	return $this->render('parental',[
    		//'content'=>$content,
    	]);
    }
    public function actionGuidance(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('guidance',[
        'content'=>$content,
      ]);
      }
    	return $this->render('guidance',[
    		//'content'=>$content,
    	]);
    }
    public function actionContactUs()
    {
    	$model = new ContactForm();
        
        $slug=Yii::$app->getRequest()->getQueryParam('slug');
        $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
        if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
        	if($content){
                \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
           }
       

        
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact-us', [
                'model' => $model,
                'content'=>$content,
            ]);
        }
         }
    }
    public function actionGetPersonalised(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('get-personalised',[
        'content'=>$content,
      ]);
      }
    	return $this->render('personalised',[
    		//'content'=>$content,
    	]);
    }
    public function actionNewsletters(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('newsletters',[
        'content'=>$content,
      ]);
      }
    	return $this->render('newsletters',[
    		//'content'=>$content,
    	]);
    }
    public function actionAdvertiseWithUs(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('advertise-with-us',[
        'content'=>$content,
      ]);
      }
    	return $this->render('advertise-with-us',[
    		//'content'=>$content,
    	]);
    }
    public function actionAdChoices(){
    	$slug=Yii::$app->getRequest()->getQueryParam('slug');
      $pageID=Pages::find()->where(['page_slug'=>$slug,'status'=>1])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('ad-choices',[
        'content'=>$content,
      ]);
      }
    	return $this->render('ad-choices',[
    		//'content'=>$content,
    	]);
    }
    //footer page actions end  

    //comments action start for news,trending,features post's
    public function actionNewComment()
    {
        $model1 = new Comments;
        $request=Yii::$app->request->post('Comments');
        $slug=$request['slug'];
        if($request['rating']==null){
          Yii::$app->session->setFlash('review-error', '
       <div class="alert alert-warning alert-dismissable">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong>Please select Stars..</strong> !</div>'
       );
          return $this->redirect(['news/view',
          'model1'=>$model1,
          'slug'=>$slug,
      ]);
        }
        $model1->post_id = $request['post_id'];
        $model1->name = $request['name'];
        $model1->comments = $request['comments'];
        $model1->category_id = $request['category_id'];
        $model1->status = $request['status'];
        if (isset($request['rating'])) {
            $model1->rating = $request['rating'];
        } else {
            $model1->rating = '0';
        }
        if (isset($request['parent_id'])) {
            $model1->parent_id = $request['parent_id'];
        } else {
            $model1->parent_id = '0';
        }
        $model1->save();
        
        Yii::$app->session->setFlash('review-create', '
       <div class="alert alert-success alert-dismissable">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong>Your Review Has been Submitted will post this soon !</strong></div>'
       );

      return $this->redirect(['news/view', 
        'model1'=>$model1,
        'slug'=>$slug,
      ]);
    }

    public function actionFeatureComment()
    {
        $model1 = new Comments;
        $request=Yii::$app->request->post('Comments');
        $slug=$request['slug'];
        if($request['rating']==null){
          Yii::$app->session->setFlash('review-error', '
       <div class="alert alert-warning alert-dismissable">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong>Please select Stars..</strong> !</div>'
       );
          return $this->redirect(['news/view', 
          'model1'=>$model1,
          'slug'=>$slug,
      ]);
        }
        $model1->post_id = $request['post_id'];
        $model1->name = $request['name'];
        $model1->comments = $request['comments'];
        $model1->category_id = $request['category_id'];
        $model1->status = $request['status'];
        if (isset($request['rating'])) {
            $model1->rating = $request['rating'];
        } else {
            $model1->rating = 0;
        }
        if (isset($request['parent_id'])) {
            $model1->parent_id = $request['parent_id'];
        } else {
            $model1->parent_id = 0;
        }
        $model1->save();
        
        Yii::$app->session->setFlash('review-create', '
       <div class="alert alert-success alert-dismissable">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong>Your Review Has been Submitted will post this soon !</strong></div>'
       );

      return $this->redirect(['feature/view', 
        'model1'=>$model1,
        'slug'=>$slug,
      ]);
    }

    public function actionTrendingNewsComment()
    {
        $model1 = new Comments;
        $request=Yii::$app->request->post('Comments');
        $slug=$request['slug'];
        if($request['rating']==null){
          Yii::$app->session->setFlash('review-error', '
       <div class="alert alert-warning alert-dismissable">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong>Please select Stars..</strong> !</div>'
       );
          return $this->redirect(['news/view', 
          'model1'=>$model1,
          'slug'=>$slug,
      ]);
        }
        $model1->post_id = $request['post_id'];
        $model1->name = $request['name'];
        $model1->comments = $request['comments'];
        $model1->category_id = $request['category_id'];
        $model1->status = $request['status'];
        if (isset($request['rating'])) {
            $model1->rating = $request['rating'];
        } else {
            $model1->rating = 0;
        }
        if (isset($request['parent_id'])) {
            $model1->parent_id = $request['parent_id'];
        } else {
            $model1->parent_id = 0;
        }
        $model1->save();
        
        Yii::$app->session->setFlash('review-create', '
       <div class="alert alert-success alert-dismissable">
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong>Your Review Has been Submitted will post this soon !</strong></div>'
       );

      return $this->redirect(['trending-news/view', 
        'model1'=>$model1,
        'slug'=>$slug,
      ]);
    }
    //comments action end

    
}