<?php
/**
 * Created by PhpStorm.
 * User: vamshi
 * Date: 27-12-2018
 * Time: 12:08
 */

namespace frontend\controllers;
use frontend\models\ArtistsData;
use frontend\models\Gallary;
use Yii;
use yii\web\Controller;
use frontend\models\GallaryCategory;
use frontend\models\Artists;


class GalleryController extends Controller
{


    public function actionIndex(){

         $category = GallaryCategory::find()->where(['gallary_cat_status'=>"ACTIVE"])->all();

        return $this->render('index',[

             'category'=>$category,

        ]);
    }


    public function actionGallery(){

        $request = Yii::$app->request;
        $id = $request->get('id');
        $category = GallaryCategory::find()->where(['gallary_cat_status'=>"ACTIVE"])->andWhere(['id'=>$id])->one();

        return $this->render('gallery',[

            'category'=>$category,

        ]);
    }


    public function actionGalleryList(){

        $request = Yii::$app->request;
        $id = $request->get('id');
        $artists = Artists::find()->where(['artist_status'=>"ACTIVE"])->andWhere(['id'=>$id])->one();


        return $this->render('gallery-list',[

            'artists'=>$artists,
            //'artistname'=>$artistname,

        ]);
    }


    public function actionGallerySection(){

        $request = Yii::$app->request;
        $id = $request->get('id');
        $artistsdataname = ArtistsData::find()->where(['artist_data_status'=>"ACTIVE"])->andWhere(['id'=>$id])->one();
        //var_dump($artistsdataname->artist_data_name);
        //exit();





        return $this->render('gallery-section',[

            'artistsdataname'=>$artistsdataname,

        ]);
    }


}