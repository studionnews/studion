<?php

namespace frontend\controllers;

use app\models\Commnets;
use Yii;
use frontend\models\News;
use frontend\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Category;
use yii\data\Pagination;
use frontend\models\Feature;
use frontend\models\FeatureSearch;
use frontend\models\Districts;
use frontend\models\Pages;
use frontend\models\PageContent;
use frontend\models\Comments;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        $model1=new Comments();
        $relateddata = News::find()->where(['news_slug'=>$slug])->one();
        $commnets = Comments::find()->where(['category_id'=>$relateddata['news_category'], 'post_id'=>$relateddata['id'],'status'=>'ACTIVE'])->all();
        //echo "<pre>";
        //print_r($commnets);
        //exit;
        // $relateddata =  $this->findModel($id);
         $cat_id=$relateddata["news_category"];
         $post_id=$relateddata["id"];
            
         $Newsdata = News::find()->where('news_category=:news_category and id != :id',['news_category'=>$cat_id,'id'=>$post_id])->andWhere (['news_status'=>'ACTIVE'])->orderBy(['id' => SORT_DESC])->limit(9)->all();


        if ($relateddata->meta_description) {
                \Yii::$app->view->registerMetaTag([
                    'name' => 'description',
                    'content' => $relateddata->meta_description
                ]);
            }
            if ($relateddata->meta_keywords) {
                \Yii::$app->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => $relateddata->meta_keywords
                ]);
            }

            
        return $this->render('view', [
            'model' => $relateddata,
            'Newsdata' => $Newsdata,
            'model1' =>$model1,
            'commnets'=>$commnets



        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionListpage($category)
    {

        $catdata = Category::find()->where(['category_slug'=>$category])->one ();
        $where=[];
        if($catdata)
        {
            $where['news_category'] = $catdata->id;
        }
        else
        {
             $district = Districts::find()->where(['slug'=>$category])->one();
             if($district){
                $where['news_districts'] = $district->id;
             }

        }

        $query = News::find();
        if(!empty($where))
        {
           $query->where($where);
        }
       

        $pagination = new Pagination([
            'defaultPageSize' => 8,
            'totalCount' => $query->count(),
        ]);


        $articles = $query->orderBy(['created_timestamp' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        

        
      $pageID=Pages::find()->where(['page_slug'=>'list-page'])->one();
      if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('listpage',[
        'content'=>$content,
        'catdata'=>$catdata,
            'articles'=>$articles,
            'pagination'=>$pagination,
      ]);
      }
        return $this->render('listpage',[
            'catdata'=>$catdata,
            'articles'=>$articles,
            'pagination'=>$pagination,
            ]);


    }

    public function actionLifestyle($slug){

        $request = Yii::$app->request;
        $catdata = Category::find()->where(['category_slug'=>$slug])->one();
        $categoryid = $catdata->id;
        $categoryname = $catdata->category_name_as;

        $pageID=Pages::find()->where(['page_slug'=>'life-style'])->one();
        if($pageID){
        $content=PageContent::find()->where(['page_id'=>$pageID->id])->andwhere(['status'=>'Active'])->one();
                if($content){
                  \Yii::$app->view->registerMetaTag ([
                   'name' => 'description',
                   'content' => $content->meta_description
               ]);
               \Yii::$app->view->registerMetaTag ([
                   'name' => 'keywords',
                   'content' => $content->meta_keywords
               ]);
                }
                return $this->render('lifestyle',[
        'content'=>$content,
        'categoryid'=>$categoryid,
             'categoryname'=>$categoryname,
      ]);
      }
        return $this->render('lifestyle',[

             'categoryid'=>$categoryid,
             'categoryname'=>$categoryname,

        ]);
    }



    
}
