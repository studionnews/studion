<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TrendingNews;
use frontend\models\TrendingNewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Comments;

/**
 * TrendingNewsController implements the CRUD actions for TrendingNews model.
 */
class TrendingNewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrendingNews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrendingNewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrendingNews model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        $model1=new Comments();
        $relateddata = TrendingNews::find()->where(['news_slug'=>$slug])->one();
        $commnets = Comments::find()->where(['post_id'=>$relateddata['id'],'status'=>'ACTIVE'])->all();
        /*echo "<pre>";
        print_r($commnets);
        exit();*/
        $cat_id=$relateddata["news_category"];
        $post_id=$relateddata["id"];
        
         $Newsdata = TrendingNews::find()->where('id != :id',['id'=>$post_id])->orderBy(['id' => SORT_DESC])->limit(9)->all();
         
         if ($relateddata->meta_description) {
                \Yii::$app->view->registerMetaTag([
                    'name' => 'description',
                    'content' => $relateddata->meta_description
                ]);
            }
            if ($relateddata->meta_keywords) {
                \Yii::$app->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => $relateddata->meta_keywords
                ]);
            }
         
        return $this->render('view', [
            'model' => $relateddata,
            'Newsdata' => $Newsdata,
            'model1' =>$model1,
            'commnets'=>$commnets
        ]);
    }

    /**
     * Creates a new TrendingNews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrendingNews();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TrendingNews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TrendingNews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrendingNews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrendingNews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrendingNews::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
