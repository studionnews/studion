
var ww = document.body.clientWidth;

$(document).ready(function() {
	$(".studio-n-toggle-menu li i").each(function() {
		if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".studio-n-toggle-menu").toggle();
	});
	adjustMenu();
})

  $(function () {
        var artaraxRatingStar = $.artaraxRatingStar({
            onClickCallBack: onRatingStar
        });
        function onRatingStar(rating) {
        	$("#comments-rating").val(rating);
            //alert("Selected rating is=" + rating);
        }
    });

$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});

var adjustMenu = function() {
	if (ww < 768) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$(".studio-n-toggle-menu").show();
		} else {
			$(".studio-n-toggle-menu").show();
		}
		$(".studio-n-toggle-menu li").unbind('mouseenter mouseleave');
		$(".studio-n-toggle-menu li i.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 768) {
		$(".toggleMenu").css("display", "none");
		$(".studio-n-toggle-menu").show();
		$(".studio-n-toggle-menu li").removeClass("hover");
		$(".studio-n-toggle-menu li i").unbind('click');
		$(".studio-n-toggle-menu li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}



function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}


function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
        h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function check()
{
	var quantity = $('#comments-rating').val();
	if (document.getElementById('comments-rating').value==""
		|| document.getElementById('comments-rating').value==undefined)
	{
		alert ("Please Select Rating");
		return false;
	}if (quantity==0) {
	alert ("Please Select Rating");
	return false;
}
	return true;
}



