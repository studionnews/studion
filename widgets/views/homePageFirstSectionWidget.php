<?php
/**
 * Created by PhpStorm.
 * User: Fingoshop
 * Date: 21-12-2018
 * Time: 11:14
 */

?>

<div class="first-sec">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="just-in-sec">
                    <h3>JUST IN</h3>
                    <ul>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                        <li><a href="#">Here's How You Can Protect Your Data On Facebook</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="studio-n-live">
                    <iframe width="100%" height="442" src="https://www.youtube.com/embed/JeMPz3-i1XA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="second-sec">
                    <div class="">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="news-post">
                                    <h3>తెలంగాణ</h3>
                                    <a href="#"><img src="images/telangana-post.jpg" alt=""/></a>
                                    <ul>
                                        <li><a href="#">12 అభ్యర్థులకు రాహుల్ ఆమోదం...</a></li>

                                    </ul>
                                    <a class="mre" href="#">మరిన్ని »</a> </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="news-post">
                                    <h3>ఆంధ్రప్రదేశ్‌</h3>
                                    <a href="#"><img src="images/ap.jpg" alt=""/></a>
                                    <ul>
                                        <li><a href="#">12 అభ్యర్థులకు రాహుల్ ఆమోదం...</a></li>

                                    </ul>
                                    <a class="mre" href="#">మరిన్ని »</a> </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="news-post">
                                    <h3>సినిమాలు </h3>
                                    <a href="#"><img src="images/sports.jpg" alt=""/></a>
                                    <ul>
                                        <li><a href="#">12 అభ్యర్థులకు రాహుల్ ఆమోదం...</a></li>

                                    </ul>
                                    <a class="mre" href="#">మరిన్ని »</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="news-oftheday"> <a href="#"> <img src="images/newsoftheday.jpg" alt=""/>

                </div>
                <div class="news-oftheday"> <a href="#"> <img src="images/newsoftheday.jpg" alt=""/>

                </div>
            </div>
        </div>
    </div>
</div>
