<?php
use yii\helpers\Url;
use frontend\widgets\headerAdWidget;
?>


<div class="header-sec">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
        <a title="Studio N" href="<?php echo Yii::$app->homeUrl?>"><h1 class="logo"><img src="<?= Yii::getAlias('@web/theme') ?>/images/logo.png" alt="studion"/></h1></a>
      </div>
      <div class="col-lg-0 col-md-0 col-sm-0 col-xs-5">
        <a href="<?php echo Yii::$app->homeUrl?>"><div class="mob-ad"><img src="<?= Yii::getAlias('@web/theme') ?>/images/154x64-Loop.gif" alt=""/></div></a>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-3">
         <?= headerAdWidget::widget() ?>
        
      </div>
    </div>
  </div>
</div>

	<div class="container">
		<div class="menu-nav">
 
          <nav>
            <input type="checkbox" id="mobile-menu-toggle" class="mobile-menu-toggle mobile-menu-toggle-button">
           <ul id="studio-n-menu" class="studio-n-toggle-menu mobile-left">
           <?php foreach ($data as $key => $value) {
               var_dump ($value['category_slug']);?>
            <li><a href="<?php echo Url::toRoute(['news/lifestyle', 'slug' => $value['category_slug']]);?>"><?php echo $value['category_name_as'];?></a></li>


          <?php } ?>

          </ul> <label class="mobile-left mobmenu-toggle" for="mobile-menu-toggle"></label>
          </nav>
          
        </div>
     </div>



