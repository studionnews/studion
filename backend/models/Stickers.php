<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_stickers}}".
 *
 * @property int $id
 * @property string $sticker_title
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Stickers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_stickers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sticker_title'], 'required'],
            [['sticker_title'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            ['created_by', 'default', 'value' =>  Yii::$app->user->identity->username],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sticker_title' => Yii::t('app', 'Sticker Title'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return StickersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StickersQuery(get_called_class());
    }
}
