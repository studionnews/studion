<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_rewrite_urls}}".
 *
 * @property int $id
 * @property string $rewrite_url
 * @property string $entiry
 * @property int $entity_id
 * @property int $status
 */
class RewriteUrls extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_rewrite_urls}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rewrite_url', 'entiry', 'entity_id', 'status'], 'required'],
            [['rewrite_url'], 'string'],
            [['entity_id', 'status'], 'integer'],
            [['entiry'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rewrite_url' => Yii::t('app', 'Rewrite Url'),
            'entiry' => Yii::t('app', 'Entiry'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return RewriteUrlsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RewriteUrlsQuery(get_called_class());
    }
}
