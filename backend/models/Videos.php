<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_videos}}".
 *
 * @property int $id
 * @property string $video_title
 * @property string $video_content
 * @property string $video_link
 * @property string $video_related_keywords
 * @property string $video_excerpt
 * @property string $video_status
 * @property int $video_homepage
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Videos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_videos}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['video_title', 'video_content', 'video_link', 'video_related_keywords', 'video_excerpt', 'video_status', 'video_homepage'], 'required'],
            [['video_title', 'video_content', 'video_link', 'video_related_keywords', 'video_excerpt', 'video_status'], 'string'],
            [['video_homepage'], 'integer'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['created_by'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'video_title' => Yii::t('app', 'Video Title'),
            'video_content' => Yii::t('app', 'Content'),
            'video_link' => Yii::t('app', 'Youtube Link'),
            'video_related_keywords' => Yii::t('app', 'Related Keywords'),
            'video_excerpt' => Yii::t('app', 'Excerpt'),
            'video_status' => Yii::t('app', 'Status'),
            'video_homepage' => Yii::t('app', 'Is Homepage Video Post'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return VideosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideosQuery(get_called_class());
    }
}
