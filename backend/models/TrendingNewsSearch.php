<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrendingNews;

/**
 * TrendingNewsSearch represents the model behind the search form of `app\models\TrendingNews`.
 */
class TrendingNewsSearch extends TrendingNews
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'news_category'], 'integer'],
            [['news_title', 'news_video', 'news_excerpt', 'news_content', 'news_slug', 'news_image1', 'news_image2', 'news_image3', 'news_status', 'created_by', 'created_timestamp', 'updated_timestamp', 'meta_title', 'meta_description', 'meta_keywords'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrendingNews::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder'=>['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'news_category' => $this->news_category,
            'created_timestamp' => $this->created_timestamp,
            'updated_timestamp' => $this->updated_timestamp,
        ]);

        $query->andFilterWhere(['like', 'news_title', $this->news_title])
            ->andFilterWhere(['like', 'news_video', $this->news_video])
            ->andFilterWhere(['like', 'news_excerpt', $this->news_excerpt])
            ->andFilterWhere(['like', 'news_content', $this->news_content])
            ->andFilterWhere(['like', 'news_slug', $this->news_slug])
            ->andFilterWhere(['like', 'news_image1', $this->news_image1])
            ->andFilterWhere(['like', 'news_image2', $this->news_image2])
            ->andFilterWhere(['like', 'news_image3', $this->news_image3])
            ->andFilterWhere(['like', 'news_status', $this->news_status])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords]);

        return $dataProvider;
    }
}
