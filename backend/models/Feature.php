<?php

namespace app\models;
use yii\behaviors\SluggableBehavior;

use Yii;

/**
 * This is the model class for table "{{%tbl_feature}}".
 *
 * @property int $id
 * @property string $feature_title
 * @property string $feature_excerpt
 * @property string $feature_content
 * @property string $feature_slug
 * @property string $feature_image
 * @property string $feature_status
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Feature extends \yii\db\ActiveRecord
{


    const ENTITY="feature/view";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_feature}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['meta_title'],
                'slugAttribute' => 'feature_slug'
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $rewriteurl = RewriteUrls::find()->where(['entity' => self::ENTITY,'entity_id'=>$this->id,'rewrite_url'=>$this->feature_slug])->one();
        if(empty($rewriteurl)) {
            $rewriteurl = new RewriteUrls();
        }
            $rewriteurl->rewrite_url = $this->feature_slug;
            $rewriteurl->entity = self::ENTITY;
            $rewriteurl->entity_id =$this->id;
            $rewriteurl->status =1;
            $rewriteurl->save (false);
            return parent::afterSave($insert, $changedAttributes);
    }








    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feature_title', 'feature_content','meta_title','category_id','feature_status'], 'required'],

            [['feature_title', 'feature_excerpt', 'feature_content', 'feature_slug', 'feature_status','meta_title','meta_description','meta_keywords'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['category_id'],'integer'],
            ['created_by', 'default', 'value' =>  Yii::$app->user->identity->username],
           // [['feature_image', 'created_by'], 'string', 'max' => 200],

            ['feature_image', 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 719,'minHeight' => 462, 'maxWidth' => 719,'maxHeight' => 462,'skipOnEmpty' => true],
            ['feature_image1', 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 546,'minHeight' => 377, 'maxWidth' => 546,'maxHeight' => 377,'skipOnEmpty' => true],
            ['feature_image2', 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 220,'minHeight' => 154, 'maxWidth' => 220,'maxHeight' => 154,'skipOnEmpty' => true],
            ['feature_image3', 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 267,'minHeight' => 356, 'maxWidth' => 267,'maxHeight' => 356,'skipOnEmpty' => true],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'feature_title' => Yii::t('app', 'Feature Title'),
            'feature_excerpt' => Yii::t('app', 'Feature Excerpt'),
            'feature_content' => Yii::t('app', 'Feature Content'),
            'feature_slug' => Yii::t('app', 'Feature Slug'),
            'feature_image' => Yii::t('app', 'Main Image (Please upload 719*462)'),
            'feature_image1' => Yii::t('app', 'Base Image (Please upload 546*377)'),
            'feature_image2' => Yii::t('app', 'Thumb Image (Please upload 220*154)'),
            'feature_image3' => Yii::t('app', 'Small Image (Please upload 267*356)'),
            'feature_status' => Yii::t('app', 'Feature Status'),
             'category_id' => Yii::t('app', 'Category'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FeatureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeatureQuery(get_called_class());
    }


    public function getCategories()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
