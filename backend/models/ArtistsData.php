<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_artists_data}}".
 *
 * @property int $artist_data_id
 * @property string $artist_data_name
 * @property int $artist_id
 * @property string $artist_data_status
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class ArtistsData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_artists_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist_data_name', 'artist_id','artist_category', 'artist_data_status'], 'required'],
            [['artist_id'], 'integer'],
            [['artist_data_status'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['artist_data_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Artist Data ID'),
            'artist_data_name' => Yii::t('app', 'Artist Data Name'),
            'artist_category' => Yii::t('app', 'Artist Category'),
            'artist_id' => Yii::t('app', 'Artist ID'),
            'artist_data_status' => Yii::t('app', 'Artist Data Status'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ArtistsDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArtistsDataQuery(get_called_class());
    }


    public function getCategories()
    {
        return $this->hasOne(\frontend\models\Artists::className(), ['id' => 'artist_id']);
    }

}
