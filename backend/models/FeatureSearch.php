<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feature;

/**
 * FeatureSearch represents the model behind the search form of `app\models\Feature`.
 */
class FeatureSearch extends Feature
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['feature_title', 'feature_excerpt', 'feature_content', 'feature_slug', 'feature_image', 'feature_status', 'created_by', 'created_timestamp', 'updated_timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feature::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder'=>['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_timestamp' => $this->created_timestamp,
            'updated_timestamp' => $this->updated_timestamp,
        ]);

        $query->andFilterWhere(['like', 'feature_title', $this->feature_title])
            ->andFilterWhere(['like', 'feature_excerpt', $this->feature_excerpt])
            ->andFilterWhere(['like', 'feature_content', $this->feature_content])
            ->andFilterWhere(['like', 'feature_slug', $this->feature_slug])
            ->andFilterWhere(['like', 'feature_image', $this->feature_image])
            ->andFilterWhere(['like', 'feature_status', $this->feature_status])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
