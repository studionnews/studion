<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TblGallaryCategory]].
 *
 * @see TblGallaryCategory
 */
class TblGallaryCategoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TblGallaryCategory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TblGallaryCategory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
