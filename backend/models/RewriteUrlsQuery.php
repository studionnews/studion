<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RewriteUrls]].
 *
 * @see RewriteUrls
 */
class RewriteUrlsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return RewriteUrls[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return RewriteUrls|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
