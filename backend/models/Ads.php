<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_ads}}".
 *
 * @property int $id
 * @property string $ad_name
 * @property string $ad_url
 * @property string $ad_image
 * @property string $ad_google
 * @property string $ad_status
 * @property string $ad_position
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Ads extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_ads}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ad_name', 'ad_status', 'ad_position'], 'required'],
            [['ad_url', 'ad_google', 'ad_status', 'ad_position'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
             ['ad_image', 'image', 'extensions' => 'jpeg, jpg, gif, png',/*'minWidth' => 1170,'minHeight' => 325, 'maxSize' => 1024 * 1024 * 2,*/'skipOnEmpty' => true],
            [['ad_name'], 'string', 'max' => 200],
            ['created_by', 'default', 'value' =>  Yii::$app->user->identity->username],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ad_name' => Yii::t('app', 'Name'),
            'ad_url' => Yii::t('app', 'Ad Url'),
            'ad_image' => Yii::t('app', 'Image'),
            'ad_google' => Yii::t('app', 'Google Ad Source Code(Description)'),
            'ad_status' => Yii::t('app', 'Status'),
            'ad_position' => Yii::t('app', 'Position'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AdsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdsQuery(get_called_class());
    }
}
