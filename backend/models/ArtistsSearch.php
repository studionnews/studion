<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Artists;

/**
 * ArtistsSearch represents the model behind the search form of `app\models\Artists`.
 */
class ArtistsSearch extends Artists
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['artist_name', 'artist_status', 'created_timestamp', 'updated_timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Artists::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder'=>['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'artist_id' => $this->id,
            'created_timestamp' => $this->created_timestamp,
            'updated_timestamp' => $this->updated_timestamp,
        ]);

        $query->andFilterWhere(['like', 'artist_name', $this->artist_name])
            ->andFilterWhere(['like', 'artist_status', $this->artist_status]);

        return $dataProvider;
    }
}
