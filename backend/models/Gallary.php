<?php

namespace app\models;

use Yii;
use app\models\GallaryCategory;

/**
 * This is the model class for table "{{%tbl_gallary}}".
 *
 * @property int $gallery_id
 * @property int $artist_id
 * @property int $artist_data_id
 * @property string $gallery_name
 * @property string $gallery_image
 * @property string $gallery_status
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Gallary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_gallary}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['artist_id','artist_data_id','gallery_name','gallery_status'], 'required'],
            [['artist_id'], 'string'],
            [['gallery_status'], 'string'],
            [['created_timestamp', 'updated_timestamp', 'artist_data_id'], 'safe'],
            ['created_by', 'default', 'value' =>  Yii::$app->user->identity->username],
            [['gallery_name'], 'string', 'max' => 200],
            [['gallery_image'], 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 719,'minHeight' => 462, 'maxWidth' => 719,'maxHeight' => 462, 'maxFiles'=>20,'skipOnEmpty' => true],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Gallery ID'),
            'artist_id' => Yii::t('app', 'Artist Name'),
            'artist_data_id' => Yii::t('app', 'Artist Related Data'),
            'gallery_name' => Yii::t('app', 'Gallery Name'),
            'gallery_image' => Yii::t('app', 'Gallery Image (Please upload 719*462)'),
            'gallery_status' => Yii::t('app', 'Gallery Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return GallaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GallaryQuery(get_called_class());
    }

    public function getTblcategory()
    {
        return $this->hasOne(Gallary::className(), ['id' => 'category_id']);
    }
}
