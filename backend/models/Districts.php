<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_districts}}".
 *
 * @property int $id
 * @property string $district_name
 * @property int $state_id
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Districts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_districts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['district_name', 'state_id'], 'required'],
            [['district_name','name_english','slug'], 'string'],
            [['state_id'], 'integer'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['created_by'], 'string', 'max' => 100],
            ['created_by', 'default', 'value' => Yii::$app->user->identity->username],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'district_name' => Yii::t('app', 'District Name in telugu'),
            'name_english'  => Yii::t('app', 'District Name in English'),
            'state_id' => Yii::t('app', 'State Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DistrictsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DistrictsQuery(get_called_class());
    }


}
