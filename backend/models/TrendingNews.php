<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;


/**
 * This is the model class for table "{{%tbl_trending_news}}".
 *
 * @property int $id
 * @property string $news_title
 * @property string $news_excerpt
 * @property string $news_content
 * @property string $news_slug
 * @property string $news_image1
 * @property string $news_image2
 * @property string $news_image3
 * @property string $news_status
 * @property int $news_category
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class TrendingNews extends \yii\db\ActiveRecord
{
    const ENTITY="trending-news/view";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_trending_news}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['meta_title'],
                'slugAttribute' => 'news_slug'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */


    public function afterSave($insert, $changedAttributes)
    {

        $rewriteurl = RewriteUrls::find()->where(['entity' => self::ENTITY,'entity_id'=>$this->id,'rewrite_url'=>$this->news_slug])->one();
        if(empty($rewriteurl)) {
            $rewriteurl = new RewriteUrls();
        }
            $rewriteurl->rewrite_url = $this->news_slug;
            $rewriteurl->entity = self::ENTITY;
            $rewriteurl->entity_id =$this->id;
            $rewriteurl->status =1;
            $rewriteurl->save (false);
            return parent::afterSave($insert, $changedAttributes);



    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_title','meta_title','news_status','news_content'], 'required'],
            [['news_title', 'news_excerpt', 'news_content', 'news_slug', 'news_status','news_video','meta_title','meta_description','meta_keywords'], 'string'],
            [['news_category'], 'integer'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['news_image2', 'news_image3', 'created_by'], 'string', 'max' => 200],
            ['news_image1', 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 719,'minHeight' => 462, 'maxWidth' => 719,'maxHeight' => 462, 'maxSize' => 1024 * 1024 * 2,'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'news_title' => Yii::t('app', 'Title'),
            'news_excerpt' => Yii::t('app', 'Excerpt'),
            'news_content' => Yii::t('app', 'Content'),
            'news_slug' => Yii::t('app', 'Slug'),
            'news_image1' => Yii::t('app', 'Image (Please upload 719*462)'),
            'news_image2' => Yii::t('app', 'News Image2'),
            'news_image3' => Yii::t('app', 'News Image3'),
            'news_status' => Yii::t('app', 'Status'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'news_category' => Yii::t('app', 'Category'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TrendingNewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrendingNewsQuery(get_called_class());
    }
}
