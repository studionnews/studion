<?php

namespace app\models;
use yii\behaviors\SluggableBehavior;

use Yii;

/**
 * This is the model class for table "{{%tbl_news}}".
 *
 * @property int $id
 * @property string $news_title
 * @property string $news_content
 * @property string $news_image1
 * @property string $news_image2
 * @property string $news_image3
 * @property string $news_status
 * @property int $news_category
 * @property string $created_by
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class News extends \yii\db\ActiveRecord
{


    const ENTITY="news/view";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_news}}';
    }


  public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['meta_title'],
                'slugAttribute' => 'news_slug'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */


    public function afterSave($insert, $changedAttributes)
    {

        $rewriteurl = RewriteUrls::find()->where(['entity' => self::ENTITY,'entity_id'=>$this->id])->one();
        if(empty($rewriteurl)) {
            $rewriteurl= new RewriteUrls();

        }
        $rewriteurl->rewrite_url = $this->news_slug;
        $rewriteurl->entity = self::ENTITY;
        $rewriteurl->entity_id =$this->id;
        $rewriteurl->status =1;
        $rewriteurl->save (false);
        return parent::afterSave($insert, $changedAttributes);

    }



    public function rules()
    {
        return [
            [['news_title','news_status', 'news_category','meta_title'], 'required'],
            //['meta_title','unique'],
            ['meta_title', 'unique', 'targetClass' => '\app\models\News', 'message' => 'Meta title .'],
            [['news_title', 'news_content', 'news_status','news_excerpt','news_slug','news_video','meta_description','meta_keywords'], 'string'],
            [['news_category'], 'integer'],
            [['news_districts'],'string'],
            [['created_timestamp', 'updated_timestamp','districts_default'], 'safe'],
            [['news_image2', 'news_image3','news_image4'], 'string', 'max' => 200],
            ['created_by', 'default', 'value' =>  Yii::$app->user->identity->username],
             ['news_image1', 'image', 'extensions' => 'jpeg, jpg, gif, png','minWidth' => 719,'minHeight' => 462, 'maxWidth' => 719,'maxHeight' => 462, /*'maxSize' => 1024 * 1024 * 2*/'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'news_title' => Yii::t('app', 'News Title'),
            'news_video' => Yii::t('app', 'Video link'),
            'news_content' => Yii::t('app', 'Content'),
            'news_slug' => Yii::t('app', 'Slug'),
            'news_excerpt' => Yii::t('app', 'Excerpt'),
            'news_image1' => Yii::t('app', 'Image (Please upload 719*462)'),
            'news_image2' => Yii::t('app', 'Image'),
            'news_image3' => Yii::t('app', 'Image'),
            'news_image4' => Yii::t('app', 'Image'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'news_status' => Yii::t('app', 'Status'),
            'news_category' => Yii::t('app', 'Category'),
            'news_districts' => Yii::t('app', 'Districts'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }


    public function getCategories()
    {
        return $this->hasOne(Category::className(), ['id' => 'news_category']);
    }
}
