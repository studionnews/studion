<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_states}}".
 *
 * @property int $id
 * @property string $state_name
 * @property int $parent_id
 * @property int $position
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_states}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_name'], 'required'],
            [['state_name'], 'string'],
            [['parent_id', 'position'], 'integer'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'state_name' => Yii::t('app', 'State Name'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'position' => Yii::t('app', 'Position'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return StatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatesQuery(get_called_class());
    }
}
