<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_page_content}}".
 *
 * @property int $id
 * @property int $page_id
 * @property string $content
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class PageContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_page_content}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'content','meta_title','meta_description','meta_keywords', 'status'], 'required'],
            [['page_id'], 'integer'],
            ['page_id','unique', 'targetClass'=>'\app\models\PageContent','message'=>'This Page Content already created please check.'],
            [['content', 'status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_id' => Yii::t('app', 'Page Name'),
            'content' => Yii::t('app', 'Content'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TblPageContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TblPageContentQuery(get_called_class());
    }
    public function getPages()
        {
            return $this->hasOne(Pages::className(), ['id' => 'page_id']);
        }
}
