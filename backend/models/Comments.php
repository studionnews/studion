<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_comments}}".
 *
 * @property int $id
 * @property int $post_id
 * @property int $category_id
 * @property string $name
 * @property int $rating
 * @property string $comments
 * @property int $parent_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_comments}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_id', 'category_id', 'name','rating','comments', 'parent_id'], 'required'],
            [['post_id', 'category_id', 'rating', 'parent_id'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['comments'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'name' => Yii::t('app', 'Name'),
            'rating' => Yii::t('app', 'Rating'),
            'comments' => Yii::t('app', 'Comments'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TblCommentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TblCommentsQuery(get_called_class());
    }
}
