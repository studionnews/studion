<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "{{%tbl_gallary_category}}".
 *
 * @property int $gallary_cat_idl
 * @property string $gallary_cat_name
 * @property string $gallary_cat_status
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class GallaryCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tbl_gallary_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gallary_cat_name', 'gallary_cat_status'], 'required'],
            [['gallary_cat_status'], 'string'],
            [['created_timestamp', 'updated_timestamp'], 'safe'],
            [['gallary_cat_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'gallary_cat_id' => Yii::t('app', 'Gallary Cat ID'),
            'gallary_cat_name' => Yii::t('app', 'Gallary Cat Name'),
            'gallary_cat_status' => Yii::t('app', 'Gallary Cat Status'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return GallaryCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GallaryCategoryQuery(get_called_class());
    }




}
