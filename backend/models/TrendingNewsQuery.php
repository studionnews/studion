<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TrendingNews]].
 *
 * @see TrendingNews
 */
class TrendingNewsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TrendingNews[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TrendingNews|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
