<?php

namespace app\models;
use yii\behaviors\SluggableBehavior;

use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $id
 * @property string $category_name
 * @property string $created_timestamp
 * @property string $updated_timestamp
 */
class Category extends \yii\db\ActiveRecord
{


    const ENTITY="news/lifestyle";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }


    /*public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['category_name_as'],
                'slugAttribute' => 'category_slug'
            ],
        ];
    }*/

    public function afterSave($insert, $changedAttributes)
    {
        $rewriteurl = RewriteUrls::find()->where(['entity' => self::ENTITY,'entity_id'=>$this->id,'rewrite_url'=>$this->category_slug])->one();
        if(empty($rewriteurl)) {
            $rewriteurl= new RewriteUrls();

        }
        $rewriteurl->rewrite_url = $this->category_slug;
        $rewriteurl->entity = self::ENTITY;
        $rewriteurl->entity_id =$this->id;
        $rewriteurl->status =1;
        $rewriteurl->save (false);
        return parent::afterSave($insert, $changedAttributes);


    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name','category_name_as'], 'required'],
            [['id'], 'integer'],
            [['category_name','category_name_as','menu_option','category_slug'], 'string'],
            [['created_timestamp', 'updated_timestamp','parent_id','position','menu_position','inner_page'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_name' => Yii::t('app', 'Category Telugu'),
            'category_name_as' => Yii::t('app', 'Category In English'),
            'menu_position' => Yii::t('app', 'Menu Position'),
            'parent_id' => Yii::t('app', 'Tabs to display'),
            'menu_option' => Yii::t('app', 'Is it menu option'),
            'position' => Yii::t('app', 'Position'),
            'inner_page' => Yii::t('app', 'Inner page Please select only one page'),
            'created_timestamp' => Yii::t('app', 'Created Timestamp'),
            'updated_timestamp' => Yii::t('app', 'Updated Timestamp'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
}
