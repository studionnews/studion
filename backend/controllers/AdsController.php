<?php

namespace backend\controllers;

use Yii;
use app\models\Ads;
use app\models\AdsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * AdsController implements the CRUD actions for Ads model.
 */
class AdsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            
        ];
    }

    /**
     * Lists all Ads models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ads model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ads();

        if ($model->load(Yii::$app->request->post())) {
            $position = $model->ad_position;


            $path=Yii::$app->basePath.'/';
            $altername =  "ads";
            $numbers = (rand(10,1000000));
            $result = $altername . '' . $numbers;
           
                $transId = $result;
                $image = UploadedFile::getInstance($model, 'ad_image');

            if(!$image)
            {
                Yii::$app->session->setFlash('create-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Warning!  Please upload post image.</strong></div>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
                if($image) {
                    $imgName = $transId . '.' . $image->getExtension ();
                    $image->saveAs (Yii::getAlias ($path . 'web/images/') . '/' . $imgName);

                    if($position=="Header") {
                        Image::thumbnail (Yii::getAlias ($path . 'web/images/') . '/' . $imgName, 1170, 160)
                            ->save (Yii::getAlias (Yii::getAlias ($path . 'web/images/resizeimages/commercial/header') . '/' . $imgName), ['quality' => 100]);
                        $model->ad_image = $imgName;
                        $model->save ();
                    }


                   else {
                        Image::thumbnail (Yii::getAlias ($path . 'web/images/') . '/' . $imgName, 360, 325)
                            ->save (Yii::getAlias (Yii::getAlias ($path . 'web/images/resizeimages/commercial/sidebar') . '/' . $imgName), ['quality' => 100]);
                        $model->ad_image = $imgName;
                        $model->save ();

                        Yii::$app->session->setFlash('success', "Ads added successfully");
                  return $this->redirect(['ads/index']);
                    }


                    Yii::$app->session->setFlash('success', "Ads added successfully");

                }

                $model->save ();

                    Yii::$app->session->setFlash('success', "Ads added successfully");
                  return $this->redirect(['ads/index']);

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel ($id);
        $oldImage = $model->ad_image;


        if ($model->load (Yii::$app->request->post ())) {
            $position = $model->ad_position;
            $path = Yii::$app->basePath . '/';
            $altername = "ads";
            $numbers = (rand (10, 1000000));
            $result = $altername . '' . $numbers;
            $transId = $result;

            $image = \yii\web\UploadedFile::getInstance ($model, 'ad_image');



            if ($image) {

                $imgName = $transId . '.' . $image->getExtension ();
                $image->saveAs (Yii::getAlias ($path . 'web/images/') . '/' . $imgName);

                if ($position == "Header") {
                    Image::thumbnail (Yii::getAlias ($path . 'web/images/') . '/' . $imgName, 1170, 160)
                        ->save (Yii::getAlias (Yii::getAlias ($path . 'web/images/resizeimages/commercial/header') . '/' . $imgName), ['quality' => 100]);
                    $model->ad_image = $imgName;
                    $model->save ();
                } else {
                    Image::thumbnail (Yii::getAlias ($path . 'web/images/') . '/' . $imgName, 360, 325)
                        ->save (Yii::getAlias (Yii::getAlias ($path . 'web/images/resizeimages/commercial/sidebar') . '/' . $imgName), ['quality' => 100]);
                    $model->ad_image = $imgName;
                    $model->save ();
                }


                Yii::$app->session->setFlash ('success', "Ads added successfully");


                $model->save ();

                Yii::$app->session->setFlash ('success', "Ad Updated successfully");
                return $this->redirect (['ads/index']);

            } else {
                    $model->ad_image = $oldImage;
                    $model->save(false);
                    Yii::$app->session->setFlash('success', "Ads updated successfully");

                }
                return $this->redirect(['index']);
            }

            return $this->render('update', [
                'model' => $model,
            ]);

        }



    /**
     * Deletes an existing Ads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ads::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
