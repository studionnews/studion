<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use \yii\helpers\Url;

/**
 * HostelsController implements the CRUD actions for Hostels model.
 */
class BaseController extends Controller
{

    public function beforeAction($action)
    {
        $allowedActions=[
            "signup",

        ];

        if ( Yii::$app->user->isGuest )
        {
            if(!(in_array($action->id,$allowedActions)) && $action->id !="login")
            {

                return $this->redirect(['admin/user/login']);
            }
        }
        return parent::beforeAction($action);


    }
}

