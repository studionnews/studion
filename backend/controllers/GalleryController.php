<?php

namespace backend\controllers;

use Yii;
use app\models\Gallery;
use app\models\GallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        $path=Yii::$app->basePath.'/';
        $altername =  $model->gallery_name;
        $numbers = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $uploadedby = Yii::$app->user->identity->username;

        if ($model->load(Yii::$app->request->post())) {
            $transId = $result;
            $image = UploadedFile::getInstance($model, 'gallery_image');
            $imgName = $transId . '.' . $image->getExtension();

            $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 321)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallery') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 163, 151)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallery/thumbnails') . '/' . $imgName), ['quality' => 100]);

            $model->created_by = $uploadedby;
            $model->gallery_image = $imgName;
            $model->save();

            Yii::$app->session->setFlash('success', "News added successfully");
            return $this->redirect(['gallery/index']);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
