<?php

namespace backend\controllers;

use Yii;
use app\models\TrendingNews;
use app\models\TrendingNewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * TrendingNewsController implements the CRUD actions for TrendingNews model.
 */
class TrendingNewsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];

    }

    /**
     * Lists all TrendingNews models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrendingNewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrendingNews model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrendingNews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrendingNews();


        $path=Yii::$app->basePath.'/';
        $altername =  "trend";
        $numbers = (rand(10,1000000));
        $result = $altername . '' . $numbers;

        if ($model->load(Yii::$app->request->post())) {
            $transId = $result;
            $image = UploadedFile::getInstance($model, 'news_image1');
            if(!$image)
            {
                Yii::$app->session->setFlash('create-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Warning!  Please upload post image.</strong></div>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }


            $meta = TrendingNews::find()->where(['news_slug'=>$model->news_slug])->count();
            if($meta>=1){
                Yii::$app->session->setFlash('meta-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Meta title is already exists please change meta title</strong></div>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            $imgName = $transId . '.' . $image->getExtension();
            $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 235, 165)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/trending') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 109, 77)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/trending/thumbs') . '/' . $imgName), ['quality' => 100]);

            $model->news_image1 = $imgName;
            $model->save();

            Yii::$app->session->setFlash('success', "Trending News added successfully");
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TrendingNews model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->news_image1;
        $path=Yii::$app->basePath.'/';
        $altername = "trend";
        $numbers = (rand(10,1000000));
        $result = $altername . '' . $numbers;

        if ($model->load(Yii::$app->request->post())) {
            $transId = $result;
            $image = UploadedFile::getInstance($model, 'news_image1');
            if($image) {
                $imgName = $transId . '.' . $image->getExtension();

                $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 235, 165)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/trending') . '/' . $imgName), ['quality' => 100]);

                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 109, 77)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/trending/thumbs') . '/' . $imgName), ['quality' => 100]);

            $model->news_image1 = $imgName;
                $model->save();


                Yii::$app->session->setFlash('success', "Feature Image updated successfully");

            } else {

                $model->news_image1 = $oldImage;
                $model->save(false);
                Yii::$app->session->setFlash('success', "Feature updated successfully");

            }


            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TrendingNews model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = TrendingNews::find()->asArray()->where('id=:id',['id'=>$id])->all();
        foreach ($data as $new) {
            $this->findModel($id)->delete();
            $newsslug = $new['news_slug'];
            RewriteUrls::deleteAll ('entity_id =:entityid AND rewrite_url=:rewriteurl',[':entityid'=>$id,':rewriteurl'=>$newsslug]);
            Yii::$app->session->setFlash('danger', "Feature News Deleted successfully");
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the TrendingNews model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrendingNews the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrendingNews::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
