<?php

namespace backend\controllers;

use app\models\RewriteUrls;
use Yii;
use app\models\Feature;
use app\models\FeatureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * FeatureController implements the CRUD actions for Feature model.
 */
class FeatureController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feature models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeatureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feature model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feature model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feature();

        $path=Yii::$app->basePath.'/';
        $altername =  "feature";
        $numbers = (rand(10,1000000));
        $number = (rand(10,1000000));
        $numb = (rand(10,1000000));
        $num = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $results = $altername . '' . $number;
        $resul = $altername . '' . $numb;
        $res = $altername . '' . $num;
        //$uploadedby = Yii::$app->user->identity->username;


        if ($model->load(Yii::$app->request->post())) {
            $transId = $result;
            $transIds = $results;
            $trans = $resul;
            $tran = $res;
            $image = UploadedFile::getInstance($model, 'feature_image');
            $image1 = UploadedFile::getInstance($model, 'feature_image1');
            $image2 = UploadedFile::getInstance($model, 'feature_image2');
            $image3 = UploadedFile::getInstance($model, 'feature_image3');

           /* if(!$image)
            {
                Yii::$app->session->setFlash('create-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Warning!  Please upload post image.</strong></div>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }*/

            $meta = Feature::find()->where(['feature_slug'=>$model->feature_slug])->count();

            if($meta>=1){
                Yii::$app->session->setFlash('meta-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Meta title is already exists please change meta title</strong></div>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }



            $imgName = $transId . '.' . $image->getExtension();
            $imgNames = $transIds . '.' . $image1->getExtension();
            $imgName3 = $trans . '.' . $image2->getExtension();
            $imgName4 = $tran . '.' . $image3->getExtension();

            $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
            $image1->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgNames);
            $image2->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName3);
            $image3->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName4);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 555, 384)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature') . '/' . $imgName), ['quality' => 100]);


            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgNames, 546, 377)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature/bigthumb') . '/' . $imgNames), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName3, 220, 154)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature/thumbnails') . '/' . $imgName3), ['quality' => 100]);


            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName4, 267, 356)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature/poster') . '/' . $imgName4), ['quality' => 100]);



            //$model->created_by = $uploadedby;
            $model->feature_image = $imgName;
            $model->feature_image1 = $imgNames;
            $model->feature_image2 = $imgName3;
            $model->feature_image3 = $imgName4;
            $model->save();

            Yii::$app->session->setFlash('success', "Feature Image added successfully");
            return $this->redirect(['feature/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Feature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $path=Yii::$app->basePath.'/';
        $altername =  "feature";
        $oldImage = $model->feature_image;
        $oldImage2 = $model->feature_image1;
        $oldImage3 = $model->feature_image2;
        $oldImage4 = $model->feature_image3;

        $numbers = (rand(10,1000000));
        $number = (rand(10,1000000));
        $numb = (rand(10,1000000));
        $num = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $results = $altername . '' . $number;
        $resul = $altername . '' . $numb;
        $res = $altername . '' . $num;
        $transId = $result;
        $transIds = $results;
        $trans = $resul;
        $tran = $res;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'feature_image');
            $image1 = UploadedFile::getInstance($model, 'feature_image1');
            $image2 = UploadedFile::getInstance($model, 'feature_image2');
            $image3 = UploadedFile::getInstance($model, 'feature_image3');


            if($image)
            {

                $imgName = $transId . '.' . $image->getExtension();
                $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 555, 384)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature') . '/' . $imgName), ['quality' => 100]);
                $model->feature_image = $imgName;
                $model->feature_image1 = $oldImage2;
                $model->feature_image2 = $oldImage3;
                $model->feature_image3 = $oldImage4;
                $model->save ();
                Yii::$app->session->setFlash('success', "Feature updated successfully");
            }
            elseif ($image1)
            {
               $imgNames = $transIds . '.' . $image1->getExtension();
               $image1->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgNames);
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgNames, 546, 377)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature/bigthumb') . '/' . $imgNames), ['quality' => 100]);
                $model->feature_image = $oldImage;
                $model->feature_image1 = $imgNames;
                $model->feature_image2 = $oldImage3;
                $model->feature_image3 = $oldImage4;
                $model->save ();
                Yii::$app->session->setFlash('success', "Feature updated successfully");
            }
            elseif($image2)
            {
                $imgName3 = $trans . '.' . $image2->getExtension();
                $image2->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName3);
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName3, 220, 154)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature/thumbnails') . '/' . $imgName3), ['quality' => 100]);
                $model->feature_image = $oldImage;
                $model->feature_image1 = $oldImage2;
                $model->feature_image2 = $imgName3;
                $model->feature_image3 = $oldImage4;
                $model->save ();
                Yii::$app->session->setFlash('success', "Feature updated successfully");
            }
            elseif ($image3)
            {
                $imgName4 = $tran . '.' . $image3->getExtension();
                $image3->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName4);
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName4, 267, 356)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/feature/poster') . '/' . $imgName4), ['quality' => 100]);
                $model->feature_image = $oldImage;
                $model->feature_image1 = $oldImage2;
                $model->feature_image2 = $oldImage3;
                $model->feature_image3 = $imgName4;
                $model->save ();
                Yii::$app->session->setFlash('success', "Feature updated successfully");
            }else{

             $model->feature_image = $oldImage;
             $model->feature_image1 = $oldImage2;
             $model->feature_image2 = $oldImage3;
             $model->feature_image3 = $oldImage4;
             $model->save(false);
                Yii::$app->session->setFlash('success', "Feature updated successfully");

        }
          return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    
    /**
     * Deletes an existing Feature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $data = Feature::find()->asArray()->where('id=:id',['id'=>$id])->all();
        foreach ($data as $new) {
            $this->findModel($id)->delete();
            $newsslug = $new['feature_slug'];
            RewriteUrls::deleteAll ('entity_id =:entityid AND rewrite_url=:rewriteurl',[':entityid'=>$id,':rewriteurl'=>$newsslug]);
            Yii::$app->session->setFlash('danger', "News Deleted successfully");
        }
        return $this->redirect(['index']);

    }

    /**
     * Finds the Feature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feature::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
