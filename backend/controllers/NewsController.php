<?php

namespace backend\controllers;

use app\models\RewriteUrls;
use Yii;
use app\models\News;
use app\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\imagine\Image;
use Imagine\Image\Box;
use app\models\Districts;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
             
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       // $dataProvider->pagination->pageSize=8;



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        $path=Yii::$app->basePath.'/';
        $altername =  "news";
        $numbers = (rand(10,1000000));
        $number = (rand(10,1000000));
        $numb = (rand(10,1000000));
        $num = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $results = $altername . '' . $number;
        $resul = $altername . '' . $numb;
        $res = $altername . '' . $num;


        if ($model->load(Yii::$app->request->post()))
        {

              $model->news_image1 = UploadedFile::getInstance($model,'news_image1');
              if(!$model->news_image1)
              {
                Yii::$app->session->setFlash('create-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Warning!  Please upload post image.</strong></div>');
                  return $this->render('create', [
                    'model' => $model,
                  ]);
              }
             

              $meta = News::find()->where(['news_slug'=>$model->news_slug])->count();
              if($meta>=1){
                Yii::$app->session->setFlash('meta-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Meta title is already exists please change meta title</strong></div>');
                  return $this->render('create', [
                    'model' => $model,
                  ]);
              }

            $transId = $result;
            $transIds = $results;
            $trans = $resul;
            $tran = $res;
            $image = UploadedFile::getInstance($model, 'news_image1');
            $imgName = $transId . '.' . $image->getExtension();

           $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
              Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 265)
         ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/home') . '/' . $imgName), ['quality' => 100]);

            $imgNames = $transIds . '.' . $image->getExtension();
         Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 719, 462)
        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/detail') . '/' . $imgNames), ['quality' => 100]);

            $imgName3 = $trans . '.' . $image->getExtension();
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 345, 254)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/listing') . '/' . $imgName3), ['quality' => 100]);

            $imgName4 = $tran . '.' . $image->getExtension();
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 458, 442)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/newsofday') . '/' . $imgName4), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 645, 365)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/latest/') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 176, 120)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/latest/thumnails/') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 262, 350)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/movies/header') . '/' . $imgName), ['quality' => 100]);


            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 238)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/movies/') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 157, 94)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/movies/thumbnails/') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 249)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/districts') . '/' . $imgName), ['quality' => 100]);

            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 149, 104)
                ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/districts/thumbs/') . '/' . $imgName), ['quality' => 100]);


            $model->news_image1 = $imgName;
            $model->news_image2 = $imgNames;
            $model->news_image3 = $imgName3;
            $model->news_image4 = $imgName4;




            $model->save();


                Yii::$app->session->setFlash('success', "News added successfully");
              return $this->redirect(['news/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->news_image1;
        $oldImage2 = $model->news_image2;
        $oldImage3 = $model->news_image3;
        $oldImage4 = $model->news_image4;
        $path=Yii::$app->basePath.'/';
        $altername =  "news";
        $numbers = (rand(10,1000000));
        $number = (rand(10,1000000));
        $numb = (rand(10,1000000));
        $num = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $results = $altername . '' . $number;
        $resul = $altername . '' . $numb;
        $res = $altername . '' . $num;




        
        if ($model->load(Yii::$app->request->post())) {


            /* $meta = News::find()->where(['news_slug'=>$model->news_slug])->count();
                if($meta>=1){
                  Yii::$app->session->setFlash('meta-error', '
                    <div class="alert alert-warning alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <strong>Meta title is already exists please change meta title</strong></div>');
                    return $this->render('create', [
                      'model' => $model,
                    ]);
                }*/
             $transId = $result;
            $transIds = $results;
            $trans = $resul;
            $tran = $res;
            $image = \yii\web\UploadedFile::getInstance($model, 'news_image1');
            if($image) {
             $imgName = $transId . '.' . $image->getExtension();
            $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
            Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 265)
        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/') . '/' . $imgName), ['quality' => 100]);
          //$imagee = UploadedFile::getInstance($model, 'news_image2');
          $imgNames = $transIds . '.' . $image->getExtension();

                $image->saveAs(Yii::getAlias($path . 'web/images/') . '/' . $imgName);
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 265)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/home') . '/' . $imgName), ['quality' => 100]);

                // Image::getImagine()->open(Yii::getAlias($path . 'web/images/') . '/' . $imgName)->thumbnail(new Box(360, 265))->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/home') . '/' . $imgName), ['quality' => 100]);

                $imgNames = $transIds . '.' . $image->getExtension();
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 719, 462)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/detail') . '/' . $imgNames), ['quality' => 100]);

                $imgName3 = $trans . '.' . $image->getExtension();
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 345, 254)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/listing') . '/' . $imgName3), ['quality' => 100]);

                $imgName4 = $tran . '.' . $image->getExtension();
                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 458, 441)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/newsofday') . '/' . $imgName4), ['quality' => 100]);

                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 645, 365)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/latest/') . '/' . $imgName), ['quality' => 100]);

                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 176, 120)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/news/latest/thumnails/') . '/' . $imgName), ['quality' => 100]);


                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 238)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/movies/') . '/' . $imgName), ['quality' => 100]);

                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 157, 94)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/movies/thumbnails/') . '/' . $imgName), ['quality' => 100]);

                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 248)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/districts') . '/' . $imgName), ['quality' => 100]);

                Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 149, 104)
                    ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/districts/thumbs/') . '/' . $imgName), ['quality' => 100]);



                $model->news_image1 = $imgName;
                $model->news_image2 = $imgNames;
                $model->news_image3 = $imgName3;
                $model->news_image4 = $imgName4;
                $model->save();

                Yii::$app->session->setFlash('success', "News updated successfully");
           
        } else {
            $model->news_image1 = $oldImage;
            $model->news_image2 = $oldImage2;
            $model->news_image3 =$oldImage3;
            $model->news_image4=$oldImage4;
            $model->save(false);
                Yii::$app->session->setFlash('success', "News updated successfully");

        }
          return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    
}

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function actionDelete($id)
    {
        $data = News::find()->asArray()->where('id=:id',['id'=>$id])->all();
        foreach ($data as $new) {
            $this->findModel($id)->delete();
            $newsslug = $new['news_slug'];
            RewriteUrls::deleteAll ('entity_id =:entityid AND rewrite_url=:rewriteurl',[':entityid'=>$id,':rewriteurl'=>$newsslug]);
            Yii::$app->session->setFlash('danger', "Page Deleted successfully");
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLists($catagory)
    {

        $subcatlist = Districts::find()->where('state_id	=:catid',['catid'=>$catagory])->orderBy(['district_name' => SORT_DESC])->all();
        if(count($subcatlist)>0)
        {
            echo '<option>select district</option>';
            foreach($subcatlist as $subcat)
            { 
                echo '<option value="'.$subcat->id.'">'.$subcat->district_name.'</option>';
            }

        }else{
            echo "<option value=' '></option>";
        }
    }


    public function actionList($catagory)
    {

        $subcatlist = Districts::find()->where('state_id	=:catid',['catid'=>$catagory])->orderBy(['district_name' => SORT_DESC])->all();
        if(count($subcatlist)>0)
        {
            //echo '<option>select district</option>';
            foreach($subcatlist as $subcat)
            {
                echo '<option value="'.$subcat->id.'">'.$subcat->district_name.'</option>';
            }

        }else{
            echo "<option value=' '></option>";
        }
    }






}
