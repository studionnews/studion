<?php

namespace backend\controllers;
use app\models\TblGallaryCategory;
use app\models\Artists;
use app\models\ArtistsData;
use Yii;
use app\models\Gallary;
use app\models\GallarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\helpers\Json;


/**
 * GallaryController implements the CRUD actions for Gallary model.
 */
class GallaryController extends Controller
{
    /**
     * {@inheritdoc}
     */



    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallary model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallary();
        $path=Yii::$app->basePath.'/';
        $altername =  "gallery";
        $numbers = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $transId = $result;


        if (Yii::$app->request->isPost) {

            $data =Yii::$app->request->post('Gallary');
            $catid = $data['category_id'];
            $artistid = $data['artist_id'];
            //$artistdata = $data['artist_data_id'];
            $gallaryname = $data['gallery_name'];
            $status = $data['gallery_status'];

            $model->gallery_image = UploadedFile::getInstances($model, 'gallery_image');
            if(!$model->gallery_image)
              {
                Yii::$app->session->setFlash('create-error', '
                  <div class="alert alert-warning alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Warning!  Please upload Gallery image.</strong></div>');
                  return $this->render('create', [
                    'model' => $model,
                  ]);
              }

            if ($model->gallery_image && $model->validate()) {

                foreach ($model->gallery_image as $file) {

                    $imgName = rand(10,1000000) . '.' . $file->extension;
                    $file->saveAs(Yii::getAlias($path . 'web/images/') . $imgName);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 321)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallery') . '/' . $imgName), ['quality' => 100]);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 163, 151)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallery/thumbnails') . '/' . $imgName), ['quality' => 100]);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 720, 482)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallary') . '/' . $imgName), ['quality' => 100]);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 259, 230)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallary/thumbnails') . '/' . $imgName), ['quality' => 100]);

                    $model = new Gallary();
                    $model->category_id=$catid;
                    $model->gallery_name = $gallaryname;
                    $model->artist_id = $artistid;
                   // $model->artist_data_id = $artistdata;
                    $model->gallery_image = $imgName;
                    $model->gallery_status = $status;
                    $model->save();

                }
            }


        return $this->redirect(['index']);
    }

            return $this->render('create', [
            'model' => $model,
            ]);

            }



/**
     * Updates an existing Gallary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_gallery_image = $model->gallery_image;
        $path=Yii::$app->basePath.'/';
        $altername =  "gallery";
        $numbers = (rand(10,1000000));
        $result = $altername . '' . $numbers;
        $transId = $result;

        if ($model->load(Yii::$app->request->post())) {

            $model->gallery_image = UploadedFile::getInstance($model,'gallery_image');
            if($model->gallery_image == '')
            {
                 $model->gallery_image = $old_gallery_image ;
                 $model->save();
            }
            else{

                $imgName = rand(10,1000000) . '.' . $file->extension;
                    $file->saveAs(Yii::getAlias($path . 'web/images/') . $imgName);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 360, 321)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallery') . '/' . $imgName), ['quality' => 100]);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 163, 151)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallery/thumbnails') . '/' . $imgName), ['quality' => 100]);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 720, 482)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallary') . '/' . $imgName), ['quality' => 100]);

                    Image::thumbnail(Yii::getAlias($path . 'web/images/') . '/' . $imgName, 259, 230)
                        ->save(Yii::getAlias(Yii::getAlias($path . 'web/images/resizeimages/gallary/thumbnails') . '/' . $imgName), ['quality' => 100]);
                        //$model = new Gallary();
                    $model->gallery_name = $gallaryname;
                    $model->artist_id = $artistid;
                    $model->artist_data_id = $artistdata;
                    $model->gallery_image = $imgName;
                    $model->gallery_status = $status;
                    $model->save();

            }


            
            return $this->redirect(['view', 'id' => $model->gallery_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }*/

    /**
     * Deletes an existing Gallary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionList($artist)
    {

        $subcatlist = Artists::find()->where('artist_id	=:catid',['catid'=>$artist])->orderBy(['artist_data_name' => SORT_DESC])->all();
        if(count($subcatlist)>0)
        {
            echo '<option value="">Select Artist Data</option>';
            foreach($subcatlist as $subcat)
            {
                echo '<option value="'.$subcat->artist_data_id.'">'.$subcat->artist_data_name.'</option>';
            }

        }else{
            echo "<option value=' '>No data</option>";
        }
    }



   /* public function actionSubcat() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = Artists::getSubCatList($cat_id);
                return json_encode(['output'=>$out, 'selected'=>'']);
            }
        }
        return json_encode(['output'=>'', 'selected'=>'']);
    }*/





    public function actionSubcat() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $out = self::getSubCatList(['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>']);
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }


    public function actionLists($catagory)
    {

        $subcatlist = Artists::find()->where('artist_category	=:catid',['catid'=>$catagory])->orderBy(['artist_name' => SORT_DESC])->all();
        if(count($subcatlist)>0)
        {
            echo '<option>select Artist</option>';
            foreach($subcatlist as $subcat)
            {
                echo '<option value="'.$subcat->id.'">'.$subcat->artist_name.'</option>';
            }

        }else{
            echo "<option value=' '></option>";
        }
    }


}
