<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ArtistsData */

/*$this->title = Yii::t('app', 'Create Artists Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Artists Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="artists-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
