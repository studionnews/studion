<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="artists-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Artists</h3>
        </div>
        <div class="box-body">
            <div class="row">




                <div class="col-xs-3">
                <?= $form->field($model, 'artist_category')->dropDownList(ArrayHelper::map(\app\models\TblGallaryCategory::find()->all(),'id','gallary_cat_name'),
                    ['prompt'=>'Select Category',
                        'onchange' => '
                        $.post("index.php?r=artists-data/lists&artist=' . '"+$(this).val(),function(data){
                        $("select#artistsdata-artist_id").html(data);
                        });'
                    ])
                ?>
                </div>

                <div class="col-xs-3">
                    <label>Artist Name</label>

                    <?php
                    echo $form->field($model, 'artist_id')->label(false)->dropDownList(
                        ['prompt'=>'Select Artist']);
                    ?>

                </div>




                <div class="col-xs-3">
                    <label>Album Name</label>
                    <?= $form->field($model, 'artist_data_name')->label(false)->textInput(['maxlength' => true]) ?>

                </div>



            </div>
            <div class="row">

                <div class="col-xs-3">
                    <label>Album status</label>

                    <?= $form->field($model, 'artist_data_status')->label(false)->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>

                </div>



            </div>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.box-body -->
</div>









