<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ArtistsData */

$this->title = Yii::t('app', 'Update Artists Data: {name}', [
    'name' => $model->artist_data_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Artists Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->artist_data_id, 'url' => ['view', 'id' => $model->artist_data_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="artists-data-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
