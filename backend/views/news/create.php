<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'Create News';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <?= Yii::$app->session->getFlash('create-error') ?>

    <?= Yii::$app->session->getFlash('meta-error') ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
