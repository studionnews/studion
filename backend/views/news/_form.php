<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\file\FileInput;
use marqu3s\summernote\Summernote;



/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
  $path=Yii::$app->basePath.'/';
?>

<div class="news-form">



     <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','enableAjaxValidation' => true]]) ?>

 <?= $form->field($model, 'news_slug')->textarea(['rows' => 1])->hiddenInput(['id'=> 'slug'])->label(false) ?>
       <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">News Posting</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                 <?= $form->field($model, 'news_title')->textarea(['rows' => 1]) ?>
                </div>

                   <div class="col-xs-3">
                     <?= $form->field($model, 'news_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'DEACTIVE' => 'DEACTIVE', ], ['prompt' => 'Select Status']) ?>
                
                </div>
                <div class="col-xs-3">

                    <?= $form->field($model, 'news_category')->dropDownList(ArrayHelper::map(\app\models\Category::find()->all(),'id','category_name'),['prompt'=>'Select Category',
                        'onchange' => '
                    $.post("index.php?r=news/lists&catagory=' . '"+$(this).val(),function(data){
                      $("select#news-news_districts").html(data);
                    });']) ?>


                </div>



                  <div class="col-xs-3">

                      <?php
                      echo $form->field($model, 'news_districts')->dropDownList(
                              ['prompt'=>'Select District']);
                      ?>

                  </div>
                  <div class="col-xs-6">
                      <?= $form->field($model, 'news_video')->textarea(['rows' => 1]) ?>
                  </div>
              
              </div>
               
                <div class="row">
                <div class="col-xs-12">

                  <?= $form->field($model, 'news_content')->widget(Summernote::className(), [
    'clientOptions' => [
        
    ]
]); ?>
                  
                </div>
            </div>
            <div class="row">
            
                <div class="col-xs-12">
                   <?php
                   echo $form->field($model, 'news_image1')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],


    'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,
            'initialPreview'=> [

               $model->news_image1 ? '<img src="/backend/images' . '/' . $model->news_image1.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
              ],
            'initialCaption'=> [ $model->news_image1 ? $model->news_image1 : null ,
                ],
        ],

  
]);?>


                <div class="row">
                    <div class="col-xs-3">
                        <?= $form->field($model, 'meta_title')->textInput(['id' => 'title']) ?>
                    </div>
                    <div class="col-xs-3">
                        <?= $form->field($model, 'meta_description')->textarea(['rows' => 1]) ?>
                    </div>
                    <div class="col-xs-3">
                        <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 1]) ?>

                    </div>
                </div>
            </div>

               
              </div>
               <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
            </div>
            <!-- /.box-body -->
          </div>

<?php
$this->registerJS('$("#title").on("keyup",function(){
                var title = $("#title").val();
                title = title.replace(/\s+/g,"-");
                var slug = title.toLowerCase();
                $("#slug").val(slug);
                })');
?>