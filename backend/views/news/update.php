<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

/*$this->title = 'Update News: ' . $model->news_title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id];*/
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


    <?php

    $this->registerJs('

$(document).ready(function(){
 $.post("index.php?r=news/list&catagory=' . '"+$("#news-news_category").val(),function(data){
                      $("select#news-news_districts").html(data);
                    })
})
')
    ?>

</div>
