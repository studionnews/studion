<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="pages-index">

<?= Html::a('Create page', ['/pages/create'], ['class'=>'btn btn-primary']) ?>
<?= Html::a('Page Content', ['/page-content/index'], ['class'=>'btn btn-primary']) ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'page_name',
            'page_slug',
            //'status',

            [
                'attribute'=>'status',
                'header'=>'status',
                //'filter' => [ '0' => 'Homepage Top Tab', '1' => 'Homepage Bottom Tab', '2' => 'Homepage Latest News Tab','3' => 'Homepage Special News Tab' ,'4' => 'Side bar popular News Tab' ],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    if($model->status == '0')
                    {
                        return '<p>Inactive</p>';
                    }
                    elseif($model->status == '1')
                    {
                        return '<p>Active</p>';
                    }
                },
            ],
            'position',

            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false

        'toolbar' =>  [
            '{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        'perfectScrollbar'=> true,
        //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
        //'showPageSummary' => true,
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Pages List</h3>',
            'type'=>'info',
           // 'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Page Names', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            //'footer'=>false,
        ],
    ]);


    ?>