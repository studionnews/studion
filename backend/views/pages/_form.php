<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">



    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'page_slug')->hiddenInput(['maxlength' => true])->label (false) ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Pages</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-3">
                    <?= $form->field($model, 'page_name')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-xs-3">
                    <?= $form->field($model, 'position')->textInput() ?>
                </div>
                <div class="col-xs-3">

                    <?= $form->field($model, 'status')->dropDownList([ '1' => 'Active', '0' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.box-body -->
</div>




<?php
$this->registerJS('$("#pages-page_name").on("keyup",function(){
               var title = $("#pages-page_name").val();
              // alert(title);
               title = title.replace(/\s+/g,"-");
               var slug = title.toLowerCase();
               $("#pages-page_slug").val(slug);
               })');
?>



