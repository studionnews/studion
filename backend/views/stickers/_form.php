<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stickers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stickers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sticker_title')->textarea(['rows' => 2]) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
