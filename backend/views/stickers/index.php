<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StickersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = 'Stickers';
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="stickers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Stickers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'sticker_title:ntext',
            [
                 'attribute' => 'sticker_title',
                 'format' => 'text',
                 'label' => 'Sticker title',
                 'contentOptions' => ['style' => 'width: 50%; white-space:pre-line;']
             ],

            'created_by',
            'created_timestamp',
            'updated_timestamp',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
