<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\States */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="states-form">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">States</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-3">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'state_name')->textarea(['rows' => 1]) ?>

                </div>


</div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
