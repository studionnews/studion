<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gallary */

$this->title = Yii::t('app', 'Update Gallary: {name}', [
    'name' => $model->gallery_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gallaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gallery_id, 'url' => ['view', 'id' => $model->gallery_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="gallary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
