<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use app\models\TblGallaryCategory;
use app\models\Artists;
use app\models\GallaryCategory;

/* @var $this yii\web\View */
/* @var $model app\models\Gallary */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="gallary-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Gallery</h3>
        </div>
        <div class="box-body">
            <div class="row">


                     <div class="col-xs-3">

                    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(\app\models\GallaryCategory::find()->where(['gallary_cat_status'=>'Active'])->all(),'id','gallary_cat_name'),['prompt'=>'Select Category',
                        'onchange' => '
                    $.post("index.php?r=gallary/lists&catagory=' . '"+$(this).val(),function(data){
                      $("select#gallary-artist_id").html(data);
                    });']) ?>


                </div>
                    <div class="col-xs-3">

                        <?php
                        echo $form->field($model, 'artist_id')->dropDownList(
                            ['prompt'=>'Select Artists']);
                        ?>



                </div>
               <!-- <div class="col-xs-3">
                    <?/*= $form->field($model, 'artist_data_id')->textInput(['maxlength' => true]) */?>
                </div>-->
               <div class="col-xs-3">
                    <?= $form->field($model, 'gallery_name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                       echo $form->field($model, 'gallery_image[]')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*','multiple' => true],
                'pluginOptions' => [
                'previewFileType' => 'image',
                'showUpload' => false,
                'initialPreview'=> [
                   $model->gallery_image ? '<img src="/backend/images' . '/' . $model->gallery_image.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
                    ],
                'initialCaption'=> [ $model->gallery_image ? $model->gallery_image : null ,
                        ],
                    ],
                ]);?>
                </div>
            </div>
                <?= $form->field($model, 'gallery_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => 'Select Status']) ?>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-3">
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <!-- /.box-body -->
</div>