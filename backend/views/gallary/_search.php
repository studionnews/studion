<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GallarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'gallery_id') ?>

    <?= $form->field($model, 'artist_id') ?>

    <?= $form->field($model, 'artist_data_id') ?>

    <?= $form->field($model, 'gallery_name') ?>

    <?= $form->field($model, 'gallery_image') ?>

    <?php // echo $form->field($model, 'gallery_status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_timestamp') ?>

    <?php // echo $form->field($model, 'updated_timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
