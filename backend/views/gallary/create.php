<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gallary */

/*$this->title = Yii::t('app', 'Create Gallary');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gallaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="gallary-create">

     <?= Yii::$app->session->getFlash('create-error') ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
