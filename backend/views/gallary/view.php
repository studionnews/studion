<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gallary */

$this->title = $model->gallery_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gallaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="gallary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php /*?> <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->gallery_id], ['class' => 'btn btn-primary']) ?> 
        <?php */?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->gallery_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'gallery_id',
            'artist_id',
            'artist_data_id',
            'gallery_name',
            'gallery_image',
            'gallery_status',
            'created_by',
            'created_timestamp',
            'updated_timestamp',
        ],
    ]) ?>

</div>
