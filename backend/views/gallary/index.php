<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GallarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = Yii::t('app', 'Gallaries');
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="gallary-index">

    <?= Html::a('Gallary Category', ['/gallary-category/index'], ['class'=>'btn btn-primary']) ?>
    <?= Html::a('Artists', ['/artists/index'], ['class'=>'btn btn-primary']) ?>
    <?= Html::a('Gallary', ['/gallary/index'], ['class'=>'btn btn-primary']) ?>
</br>
<?php Pjax::begin(); ?>
<?php

echo GridView::widget([
    'dataProvider'=> $dataProvider,
    //'filterModel' => $searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'bordered' => true,
    //'responsive'=>true,

    'hover'=>true,
    'resizableColumns'=>true,
    'pager' => [
        'class' => 'yii\widgets\LinkPager',
        'linkOptions' => ['class' => 'item-link']
    ],

    // 'pager' => [
    ///    'options'=>['class'=>'pagination'],   // set clas name used in ui list of
    // ],

    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Category Name',
            'value' => 'tblcategory.gallary_cat_name',
        ],


        'gallery_name',
        
        [
            'attribute' => 'gallery_image',
            'label'=> 'Gallery Image',
            'format' => 'html',
            'value' => function ($data) {
                return Html::img(Yii::getAlias('@web').'/images/'. $data['gallery_image'],
                    ['width' => '50px']);
            },
        ],

        'gallery_status',
        /*[
            'label' => 'Artists',
            'value' => 'artist.artist_name',
        ],*/



        ['class' => 'yii\grid\ActionColumn','template' => '{view} {delete}'],
    ],
    'pjax' => true,
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => true,
    'hover' => true,
    'floatHeader' => true,
    'perfectScrollbar'=> true,

    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Gallery</h3>',
        'type'=>'info',
        'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Gallery', ['create'], ['class' => 'btn btn-success']),

        'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
        //'footer'=>false,

    ],

]);
?>


    <?php Pjax::end(); ?>
</div>
