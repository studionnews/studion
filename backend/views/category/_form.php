<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'category_slug')->hiddenInput()->label (false) ?>
       <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Category</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                 <?= $form->field($model, 'category_name')->textInput() ?>
                </div>


                <div class="col-xs-3">
                 <?= $form->field($model, 'category_name_as')->textInput() ?>
                </div>

                <div class="col-xs-3">
                 <?= $form->field($model, 'menu_position')->textInput() ?>
                </div>

                  <div class="col-xs-4">
                   <?= $form->field($model, 'parent_id')->dropDownList([ '0' => 'Homepage Top Tab', '1' => 'Homepage Bottom Tab', '2' => 'Homepage Latest News Tab','3' => 'Homepage Special News Tab' ,'4' => 'Side bar popular News Tab'  ], ['prompt' => 'Select Parent Place']) ?>
                
                </div>
                  <div class="col-xs-3">
                      <?= $form->field($model, 'position')->dropDownList([ '3' => 'Left', '2' => 'Middle', '1' => 'Homepage Right',], ['prompt' => 'Select News Position']) ?>

                  </div>

              </div>
               <div class="row">
                   <div class="col-xs-3">
                       <?= $form->field($model, 'menu_option')->checkbox() ?>
                   </div>

                   <div class="col-xs-3">
                       <?= $form->field($model, 'inner_page')->checkbox() ?>
                   </div>

               
              </div>
               <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
            </div>


    <?php ActiveForm::end(); ?>

</div>
            <!-- /.box-body -->
          </div>




<?php
$this->registerJS('$("#category-category_name_as").on("keyup",function(){
               var title = $("#category-category_name_as").val();
              // alert(title);
               title = title.replace(/\s+/g,"-");
               var slug = title.toLowerCase();
               $("#category-category_slug").val(slug);
               })');
?>
   

   

   

    
