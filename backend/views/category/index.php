<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Categories';
//$this->params['breadcrumbs'][] = $this->title;
?>


<div class="category-index">


     <?php Pjax::begin(); ?>
     <?php

    echo GridView::widget([
        'dataProvider'=> $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'bordered' => true,
        //'responsive'=>true,

    'hover'=>true,
    'resizableColumns'=>true,
    'pager' => [
             'class' => 'yii\widgets\LinkPager',
             'linkOptions' => ['class' => 'item-link']
        ],

   // 'pager' => [
                ///    'options'=>['class'=>'pagination'],   // set clas name used in ui list of 
           // ],      

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category_name',
            'category_name_as',
            'menu_position',
            //'parent_id',
            [
                'attribute'=>'parent_id',
                'header'=>'Tabs to display',
                'filter' => [ '0' => 'Homepage Top Tab', '1' => 'Homepage Bottom Tab', '2' => 'Homepage Latest News Tab','3' => 'Homepage Special News Tab' ,'4' => 'Side bar popular News Tab' ],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    if($model->parent_id == '0')
                    {
                        return '<p>Homepage Top Tab</p>';
                    }
                    elseif($model->parent_id == '1')
                    {
                        return '<p>Homepage Bottom Tab</p>';
                    }
                    elseif($model->parent_id == '2')
                    {
                        return '<p>Homepage Latest News Tab</p>';
                    }
                    elseif($model->parent_id == '3')
                    {
                        return '<p>Homepage Special News Tab</p>';
                    }
                    elseif($model->parent_id == '4')
                    {
                        return '<p>Side bar popular News Tab</p>';
                    }
                },
            ],


            ['class' => 'yii\grid\ActionColumn','template' => '{view} {update} {delete}'],
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        'perfectScrollbar'=> true,

        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Category List</h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Category', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            //'footer'=>false,
            
        ],

    ]);
    ?>




    <?php Pjax::end(); ?>
</div>

