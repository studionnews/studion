<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Videos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videos-form">

    <?php $form = ActiveForm::begin(); ?>


      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Upload Videos</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                  <?= $form->field($model, 'video_title')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-4">
                     <?= $form->field($model, 'video_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
                 
                </div>
                <div class="col-xs-5">
                  <?= $form->field($model, 'video_link')->textarea(['rows' => 1]) ?>
                </div>
              </div>
               <div class="row">
                <div class="col-xs-3">
                   <?= $form->field($model, 'video_related_keywords')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-4">
                  <?= $form->field($model, 'video_excerpt')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-5">
                    <?= $form->field($model, 'video_content')->textarea(['rows' => 6]) ?>
                </div>
              </div>
               <div class="row">
                <div class="col-xs-3">
                   <?= $form->field($model, 'video_homepage')->checkbox() ?>
                </div>

              </div>

            </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
            <!-- /.box-body -->
          </div>



   

   

   

   

   

   

   


