<?php


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VideosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videos-index">



      <?php Pjax::begin(); ?>
     <?php

    echo GridView::widget([
        'dataProvider'=> $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'bordered' => true,
        //'responsive'=>true,

    'hover'=>true,
    'resizableColumns'=>true,
    'pager' => [
                    'options'=>['class'=>'pagination'],   // set clas name used in ui list of 
            ],      

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'video_title:ntext',
            //'video_content:ntext',
            'video_link:ntext',
            'video_related_keywords:ntext',
            //'video_excerpt:ntext',
            'video_status',
            'video_homepage',
            'created_by',
            'created_timestamp',
            //'updated_timestamp',

            ['class' => 'yii\grid\ActionColumn','template' => '{view} {update} {delete}'],
        ],
        'pjax'=>true,

        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Videos List</h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Videos', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            'footer'=>false,
            
        ],

    ]);
    ?>




    <?php Pjax::end(); ?>
</div>




