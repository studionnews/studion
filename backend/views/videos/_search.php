<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VideosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="videos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'video_title') ?>

    <?= $form->field($model, 'video_content') ?>

    <?= $form->field($model, 'video_link') ?>

    <?= $form->field($model, 'video_related_keywords') ?>

    <?php // echo $form->field($model, 'video_excerpt') ?>

    <?php // echo $form->field($model, 'video_status') ?>

    <?php // echo $form->field($model, 'video_homepage') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_timestamp') ?>

    <?php // echo $form->field($model, 'updated_timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
