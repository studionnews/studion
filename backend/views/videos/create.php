<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Videos */

/*$this->title = 'Create Videos';
$this->params['breadcrumbs'][] = ['label' => 'Videos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="videos-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
