<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblGallaryCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-gallary-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Artists</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-3">

    <?= $form->field($model, 'gallary_cat_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-xs-3">

    <?= $form->field($model, 'gallary_cat_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Category']) ?>
                </div>
            </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
