<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblGallaryCategory */

/*$this->title = Yii::t('app', 'Create Tbl Gallary Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tbl Gallary Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="tbl-gallary-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
