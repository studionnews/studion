<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblGallaryCategory */

$this->title = Yii::t('app', 'Update Tbl Gallary Category: {name}', [
    'name' => $model->gallary_cat_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tbl Gallary Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gallary_cat_id, 'url' => ['view', 'id' => $model->gallary_cat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tbl-gallary-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
