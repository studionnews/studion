<?php


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="trending-index">



    <?php


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'feature_title:ntext',
            //'feature_excerpt:ntext',
            //'feature_content:ntext',
            //'feature_slug:ntext',
            //'feature_image',
            //'feature_status',
            //'created_by',
            //'created_timestamp',
            //'updated_timestamp',

            [
                'label' => 'category',
                'value' => 'categories.category_name_as',
            ],

            'feature_status',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::getAlias('@web').'/images/resizeimages/feature/'. $data['feature_image'],
                        ['width' => '50px']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false

        'toolbar' =>  [
            '{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        'perfectScrollbar'=> true,
        //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
        //'showPageSummary' => true,
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Feature List</h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Feature', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            //'footer'=>false,
        ],
    ]);


    ?>