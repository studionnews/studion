<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Feature */

/*$this->title = 'Create Feature';
$this->params['breadcrumbs'][] = ['label' => 'Features', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="feature-create">

    <?= Yii::$app->session->getFlash('create-error') ?>

    <?= Yii::$app->session->getFlash('meta-error') ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
