<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\file\FileInput;
use marqu3s\summernote\Summernote;

/* @var $this yii\web\View */
/* @var $model app\models\Feature */
/* @var $form yii\widgets\ActiveForm */
?>

<?php  $data=ArrayHelper::map(app\models\Category::find()->all(), 'id', 'category_name' );
 $data1=ArrayHelper::map(app\models\Districts::find()->all(), 'id', 'district_name' );
?>

<div class="feature-form">



    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">News Posting</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-3">

    <?= $form->field($model, 'feature_title')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-3">
                    <?php echo $form->field($model, 'category_id')->widget(Select2::classname(), [
                        'data' => $data,
                        'options' => ['placeholder' => 'Select a Category'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'feature_slug')->hiddenInput(['rows' => 1])->label (false) ?>
    <?= $form->field($model, 'feature_excerpt')->hiddenInput(['rows' => 1])->label (false) ?>
                </div>

            </div>
                <div class="row">
                    <div class="col-xs-12">
    <?= $form->field($model, 'feature_content')->widget(Summernote::className(), [
        'clientOptions' => [

        ]
    ]); ?>
                    </div>
                </div>





    <?php
            echo $form->field($model, 'feature_image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],


                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'showUpload' => false,
                    'initialPreview'=> [

                        $model->feature_image ? '<img src="/backend/images/resizeimages/feature' . '/' . $model->feature_image.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
                    ],
                    'initialCaption'=> [ $model->feature_image ? $model->feature_image : null ,
                    ],
                ],


            ]);?>
            <?php
            echo $form->field($model, 'feature_image1')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],


                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'showUpload' => false,
                    'initialPreview'=> [

                        $model->feature_image1 ? '<img src="/backend/images/resizeimages/feature/bigthumb' . '/' . $model->feature_image1.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
                    ],
                    'initialCaption'=> [ $model->feature_image1 ? $model->feature_image1 : null ,
                    ],
                ],


            ]);?>
            <?php
            echo $form->field($model, 'feature_image2')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],


                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'showUpload' => false,
                    'initialPreview'=> [

                        $model->feature_image2 ? '<img src="/backend/images/resizeimages/feature/thumbnails' . '/' . $model->feature_image2.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
                    ],
                    'initialCaption'=> [ $model->feature_image2 ? $model->feature_image2 : null ,
                    ],
                ],


            ]);?>
            <?php
            echo $form->field($model, 'feature_image3')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],


                'pluginOptions' => [
                    'previewFileType' => 'image',
                    'showUpload' => false,
                    'initialPreview'=> [

                        $model->feature_image3 ? '<img src="/backend/images/resizeimages/feature/poster' . '/' . $model->feature_image3.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
                    ],
                    'initialCaption'=> [ $model->feature_image3 ? $model->feature_image3 : null ,
                    ],
                ],


            ]);?>


    <div class="row">
        <div class="col-xs-3">
            <?= $form->field($model, 'meta_title')->textInput() ?>
        </div>
        <div class="col-xs-3">
            <?= $form->field($model, 'meta_description')->textarea(['rows' => 1]) ?>
        </div>
        <div class="col-xs-3">
            <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 1]) ?>
        </div>
        <div class="col-xs-3">
        <?= $form->field($model, 'feature_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'DEACTIVE' => 'DEACTIVE', ], ['prompt' => 'Select Status']) ?>
        </div>
    </div>



    


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJS('$("#feature-meta_title").on("keyup",function(){
               var title = $("#feature-meta_title").val();
              // alert(title);
               title = title.replace(/\s+/g,"-");
               var slug = title.toLowerCase();
               $("#feature-feature_slug").val(slug);
               })');
?>