<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PageContent */

$this->title = Yii::t('app', 'Create Page Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
