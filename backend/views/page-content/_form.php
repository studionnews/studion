<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use marqu3s\summernote\Summernote;
use yii\helpers\ArrayHelper;
use app\models\Pages;


/* @var $this yii\web\View */
/* @var $model app\models\PageContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-content-form">




    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Pages</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-6">
                    <?php
                    $pageNames=Pages::find()->all();
                    $listData=ArrayHelper::map($pageNames,'id','page_name');
                    echo $form->field($model, 'page_id')->dropDownList(
                        $listData,
                        ['prompt'=>'Select Page name..']
                    );
                    ?>


                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
                </div>
            </div>
                <div class="row">
                <div class="col-xs-12">
                    <?= $form->field($model, 'content')->widget(Summernote::className(), [
                        'clientOptions' => [ ] ]); ?>


                </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <?= $form->field($model, 'meta_title')->textInput() ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($model, 'meta_description')->textarea(['rows' => 1]) ?>
                    </div>
                    <div class="col-xs-4">
                        <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 1]) ?>

                    </div>
                </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            </div>



            <?php ActiveForm::end(); ?>

        </div>
        <!-- /.box-body -->
    </div>



