<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AdsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Ads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ads-index">



     <?php Pjax::begin(); ?>
     <?php

     echo GridView::widget([
         'dataProvider' => $dataProvider,
         // 'filterModel' => $searchModel,
         'columns' => [
             ['class' => 'yii\grid\SerialColumn'],

             'ad_name',
             'ad_url:ntext',
             'ad_image',
             //'ad_google:ntext',
             'ad_status',
             'ad_position',
             'created_by',
             'created_timestamp',
             //'updated_timestamp',

             ['class' => 'yii\grid\ActionColumn'],
         ],
         'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false

         'toolbar' =>  [
             '{export}',
             '{toggleData}'
         ],
         'pjax' => true,
         'bordered' => true,
         'striped' => false,
         'condensed' => false,
         'responsive' => true,
         'hover' => true,
         'floatHeader' => true,
         'perfectScrollbar'=> true,
         //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
         //'showPageSummary' => true,
         'panel' => [
             'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Ads List</h3>',
             'type'=>'info',
             'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Ads', ['create'], ['class' => 'btn btn-success']),

             'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
             'footer'=>false,
         ],
     ]);


     ?>






    <?php Pjax::end(); ?>
</div>



   
