<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Ads */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="ads-form">
 <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Ads Posting</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                   <?= $form->field($model, 'ad_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-xs-5">
                   <?= $form->field($model, 'ad_url')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-4">
                  <?= $form->field($model, 'ad_position')->dropDownList([ 'Header' => 'Header', 'HeaderSection1' => 'HeaderSection1','HeaderSection2' => 'HeaderSection2', 'Sidebar' => 'Sidebar-up', 'SidebarMiddle' => 'Sidebar Middle','SidebarDown' => 'Sidebar Down','ContentMiddle' => 'Content Middle','ContentDown' => 'Content Down',], ['prompt' => 'Select Position']) ?>
                </div>
              </div>
                <div class="row">
                <div class="col-xs-3">
                     <?= $form->field($model, 'ad_status')->dropDownList([ 'Active' => 'Active', 'Deactive' => 'Deactive', ], ['prompt' => 'Select Status']) ?>
                   
                </div>
                <div class="col-xs-5">
   

     <?= $form->field($model, 'ad_google')->textarea(['rows' => 1]) ?>
                   
                </div>
               
              </div>


                              <div class="row">
                <div class="col-xs-12">


                     <?php
                     echo $form->field($model, 'ad_image')->widget(FileInput::classname(), [
                         'options' => ['accept' => 'image/*'],


                         'pluginOptions' => [
                             'previewFileType' => 'image',
                             'showUpload' => false,
                             'initialPreview'=> [

                                 $model->ad_image ? '<img src="/backend/images' . '/' . $model->ad_image.'" class="file-preview-image" style="width:auto;height:auto;max-width:100%;max-height:100%;">' : null,
                             ],
                             'initialCaption'=> [ $model->ad_image ? $model->ad_image : null ,
                             ],
                         ],


                     ]);?>

              </div>
            </div>
             <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
            <!-- /.box-body -->
          </div>



   

   

   

    
  



    

 

   


   


   

