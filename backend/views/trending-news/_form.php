<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\file\FileInput;
use marqu3s\summernote\Summernote;

/* @var $this yii\web\View */
/* @var $model app\models\TrendingNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trending-news-form">


    <?php  $data=ArrayHelper::map(app\models\Category::find()->all(), 'id', 'category_name' );
    ?>





    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Trending News</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-3">
                    <?= $form->field($model, 'news_title')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'news_status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'DEACTIVE' => 'DEACTIVE', ], ['prompt' => 'Select Status']) ?>

                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, 'news_video')->textarea(['rows' => 1]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <?= $form->field($model, 'news_content')->widget(Summernote::className(), [
                        'clientOptions' => [

                        ]
                    ]); ?>

                </div>
            </div>
            <div class="row">

                <div class="col-xs-12">

                    <?php
                    echo $form->field($model, 'news_image1')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],


                        'pluginOptions' => [
                            'previewFileType' => 'image',
                            'showUpload' => false,
                            'initialPreview'=> [

                                $model->news_image1 ? '<img src="/backend/images/resizeimages/trending/thumbs' . '/' . $model->news_image1.'" class="file-preview-image">' : null,
                                /*'<img src="/backend/images/resizeimages/news/news/thumbs' . '/' . $model->news_image1.'" class="file-preview-image">',*/
                            ],
                            'initialCaption'=> [ $model->news_image1 ? $model->news_image1 : null ,
                            ],
                        ],


                    ]);?>

                </div>



            </div>

            <div class="row">
                <div class="col-xs-3">
                    <?= $form->field($model, 'meta_title')->textInput() ?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'meta_description')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 1]) ?>

                </div>
                <div class="col-xs-3">
                <?= $form->field($model, 'news_slug')->hiddenInput(['rows' => 1])->label (false) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <!-- /.box-body -->
</div>


<?php
$this->registerJS('$("#trendingnews-meta_title").on("keyup",function(){
               var title = $("#trendingnews-meta_title").val();
              // alert(title);
               title = title.replace(/\s+/g,"-");
               var slug = title.toLowerCase();
               $("#trendingnews-news_slug").val(slug);
               })');
?>
