<?php


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trending-index">



    <?php


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'news_title:ntext',
            //'news_excerpt:ntext',
            //'news_content:ntext',
            [
                'attribute' => 'news_content',
                'format' => 'text',
                'label' => 'News Content',
                'contentOptions' => ['style' => 'width: 50%; white-space:pre-line;']
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
        'containerOptions' => ['style'=>'overflow: auto'], // only set when $responsive = false

        'toolbar' =>  [
            '{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        'perfectScrollbar'=> true,
        //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
        //'showPageSummary' => true,
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>News List</h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  News', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            //'footer'=>false,
        ],
    ]);


    ?>

