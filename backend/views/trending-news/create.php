<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrendingNews */

/*$this->title = Yii::t('app', 'Create Trending News');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trending News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="trending-news-create">


    	    <?= Yii::$app->session->getFlash('meta-error') ?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
