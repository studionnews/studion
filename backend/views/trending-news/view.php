<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrendingNews */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trending News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trending-news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'news_title:ntext',
            'news_video:ntext',
            'news_excerpt:ntext',
            'news_content:ntext',
            'news_slug:ntext',
            'news_image1',
            'news_image2',
            'news_image3',
            'news_status',
            'news_category',
            'created_by',
            'created_timestamp',
            'updated_timestamp',
            'meta_title',
            'meta_description:ntext',
            'meta_keywords:ntext',
        ],
    ]) ?>

</div>
