<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="artists-form">
    <?php  $data=ArrayHelper::map(app\models\TblGallaryCategory::find()->all(), 'id', 'gallary_cat_name' );?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Artists</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-3">

                    <?php echo $form->field($model, 'artist_category')->widget(Select2::classname(), [
                        'data' => $data,
                        'options' => ['placeholder' => 'Select Category'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>

                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'artist_name')->textInput(['maxlength' => true]) ?>
                </div>



                <div class="col-xs-3">
                    <?= $form->field($model, 'artist_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>

                </div>



            </div>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.box-body -->
</div>













