<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ArtistsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = Yii::t('app', 'Artists');
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="artists-index">

    <?= Html::a('Gallary Category', ['/gallary-category/index'], ['class'=>'btn btn-primary']) ?>
    <?= Html::a('Artists', ['/artists/index'], ['class'=>'btn btn-primary']) ?>
    <?= Html::a('Gallary', ['/gallary/index'], ['class'=>'btn btn-primary']) ?>
     </br>

    <?php Pjax::begin(); ?>
    <?php

    echo GridView::widget([
        'dataProvider'=> $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'bordered' => true,
        //'responsive'=>true,

        'hover'=>true,
        'resizableColumns'=>true,
        'pager' => [
            'class' => 'yii\widgets\LinkPager',
            'linkOptions' => ['class' => 'item-link']
        ],

        // 'pager' => [
        ///    'options'=>['class'=>'pagination'],   // set clas name used in ui list of
        // ],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'label' => 'Artist Name',
                'value' => 'category.gallary_cat_name',
            ],
            'artist_name',
            //'artist_category',
            'artist_status',
            'created_timestamp',


            ['class' => 'yii\grid\ActionColumn','template' => '{view} {update} {delete}'],
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        'perfectScrollbar'=> true,

        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Artist List</h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Artist', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            //'footer'=>false,

        ],

    ]);
    ?>




    <?php Pjax::end(); ?>
</div>





