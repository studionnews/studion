<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Artists */

$this->title = Yii::t('app', 'Update Artists: {name}', [
    'name' => $model->artist_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Artists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->artist_id, 'url' => ['view', 'id' => $model->artist_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="artists-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
