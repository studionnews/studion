<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ArtistsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="artists-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'artist_id') ?>

    <?= $form->field($model, 'artist_name') ?>

    <?= $form->field($model, 'artist_status') ?>

    <?= $form->field($model, 'created_timestamp') ?>

    <?= $form->field($model, 'updated_timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
