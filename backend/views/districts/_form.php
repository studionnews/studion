<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\States;
/* @var $this yii\web\View */
/* @var $model app\models\Districts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="districts-form">
    <?php  $data=ArrayHelper::map(app\models\Category::find()->all(), 'id', 'category_name' );?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Districts</h3>
    </div>
    <div class="box-body">
    <div class="row">
    <div class="col-xs-3">

    <?= $form->field($model, 'district_name')->textarea(['rows' => 1]) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'name_english')->textarea(['rows' => 1]) ?>
    </div>
    <div class="col-xs-3">
    <?= $form->field($model, 'slug')->textarea(['rows' => 1]) ?>
    </div>
    </div>
        <div class="row">
    <div class="col-xs-3">

    <?php echo $form->field($model, 'state_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => 'Select a State'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    </div>
        </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$this->registerJS('$("#districts-name_english").on("keyup",function(){
               var title = $("#districts-name_english").val();
              // alert(title);
               title = title.replace(/\s+/g,"-");
               var slug = title.toLowerCase();
               $("#districts-slug").val(slug);
               })');
?>