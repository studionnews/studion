<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Districtssearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Districts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="districts-index">

    <?php Pjax::begin(); ?>
    <?php

    echo GridView::widget([
        'dataProvider'=> $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'bordered' => true,
        //'responsive'=>true,

        'hover'=>true,
        'resizableColumns'=>true,
        'pager' => [
            'class' => 'yii\widgets\LinkPager',
            'linkOptions' => ['class' => 'item-link']
        ],

        // 'pager' => [
        ///    'options'=>['class'=>'pagination'],   // set clas name used in ui list of
        // ],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'district_name:ntext',
            'state_id',
            'created_by',
            'created_timestamp',
            [
                'attribute'=>'state_id',
                'header'=>'Tabs to display',
                'filter' => [ '0' => 'Homepage Top Tab', '1' => 'Homepage Bottom Tab', '2' => 'Homepage Latest News Tab','3' => 'Homepage Special News Tab' ,'4' => 'Side bar popular News Tab' ],
                'format'=>'raw',
                'value' => function($model, $key, $index)
                {
                    if($model->state_id == '0')
                    {
                        return '<p>Homepage Top Tab</p>';
                    }
                    elseif($model->state_id == '1')
                    {
                        return '<p>Homepage Bottom Tab</p>';
                    }

                },
            ],


            ['class' => 'yii\grid\ActionColumn','template' => '{view} {update} {delete}'],
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        'perfectScrollbar'=> true,

        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Category List</h3>',
            'type'=>'info',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i>  Category', ['create'], ['class' => 'btn btn-success']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Data', ['index'], ['class' => 'btn btn-info']),
            //'footer'=>false,

        ],

    ]);
    ?>




    <?php Pjax::end(); ?>
</div>


